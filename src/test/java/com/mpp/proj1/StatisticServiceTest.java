package com.mpp.proj1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.mpp.proj1.dto.DeviceDTO;
import com.mpp.proj1.dto.DeviceHistoryDTO;
import com.mpp.proj1.functional.StatisticFunctional;
import com.mpp.proj1.model.Device;
import com.mpp.proj1.model.DeviceHistory;
import com.mpp.proj1.model.Gateway;
import com.mpp.proj1.model.Switch;
import com.mpp.proj1.model.User;
import com.mpp.proj1.repository.DeviceHistoryRepository;
import com.mpp.proj1.repository.DeviceRepository;
import com.mpp.proj1.repository.UserRepository;

/**
 * http://localhost:8080/device-history/test-find-top-active-device?k=2
 * http://localhost:8080/device-history/test-find-dev-history?gatewayId=gw-bob-001&deviceId=switch-001&from=2019-09-01T13:34:00.000&to=2019-09-28T13:34:00.000
 * http://localhost:8080/device-history/test-count-dev-operations?gatewayId=gw-bob-001&deviceId=switch-001&from=2019-09-01T13:34:00.000&to=2019-09-28T13:34:00.000 
 *
 */

@RunWith(MockitoJUnitRunner.class)
public class StatisticServiceTest {
	
	private final SimpleDateFormat formatter = 
			new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
	
	private final String GwId = "gw-bob-001";
	private final String DeviceId = "switch-001";
	private final String DeviceId2 = "switch-002";
	private final String UserName = "admin";
	
	@Mock
	DeviceHistoryRepository devHistoryRepo;
	
	@Mock
	UserRepository userRepo;
	
	@Mock
	DeviceRepository devRepo;
	
//	@InjectMocks
//	StatisticServiceImpl statisticService;
	
	List<DeviceHistory> histories;
	List<User> users;
	List<Device> devices;
	
	
	@Before
	public void Setup() {
		
		histories = new ArrayList<DeviceHistory>();
		
		histories.add(new DeviceHistory(GwId, DeviceId, true, new Date()));
		histories.add(new DeviceHistory(GwId, DeviceId, false, new Date()));
		
		users = new ArrayList<User>();
		
		User admin = new User(1, "admin", "****", "Admin", 0, "", null, null);
		
		Set<Gateway> gws = new HashSet<Gateway>();
		Gateway gw = new Gateway();
		gw.setName("");
		gw.setGatewayId(GwId);
		
		devices = new ArrayList<Device>();
		devices.add(new Switch("Switch 01", GwId, DeviceId));
		devices.add(new Switch("Switch 02", GwId, DeviceId2));
		gw.setDevices(devices);
		
		gws.add(gw);
		admin.setGateways(gws);
		
		users.add(admin);
	}
	
//	public static void main(String[] args) {
//		
//		SimpleDateFormat formatter1 = 
//				new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
//		Date from = null, to = null;
//		try {
//			from = formatter1.parse("2019-09-01");
//			to = formatter1.parse("2019-10-01");
//		} catch (ParseException e) {}
//		
//		System.out.println(from);
//		System.out.println(to);
//	}

	@Test
	public void testFindDeviceHistory() {
		
		Date from = null, to = null;
		try {
			from = formatter.parse("2019-09-01");
			to = formatter.parse("2019-10-01");
		} catch (ParseException e) {}
		
		
		when(devHistoryRepo.findAll()).thenReturn(histories);
		
		List<DeviceHistoryDTO> devHistory = StatisticFunctional.FnFindDeviceHistory
		.apply(devHistoryRepo.findAll(), GwId, DeviceId, from, to);
		
		assertNotNull(devHistory);
		assertEquals(2, devHistory.size());
			
	}
	
	@Test
	public void testCountDeviceHistory() {
		
		Date from = null, to = null;
		try {
			from = formatter.parse("2019-09-01");
			to = formatter.parse("2019-10-01");
		} catch (ParseException e) {}
		
		when(devHistoryRepo.findAll()).thenReturn(histories);
		
		int nb = StatisticFunctional.FnCountDeviceOperations.apply(
				devHistoryRepo.findAll(), GwId, DeviceId, from, to).get();
		
		assertEquals(2, nb);		
	}
	
	@Test
	public void testCountDeviceOperationsByUser() {
		
		Date from = null, to = null;
		try {
			from = formatter.parse("2019-09-01");
			to = formatter.parse("2019-10-01");
		} catch (ParseException e) {}

		when(devHistoryRepo.findAll()).thenReturn(histories);
		when(userRepo.findByUsername(UserName)).thenReturn(users.get(0));
		
		User u = userRepo.findByUsername(UserName);
		int nb = StatisticFunctional.FnCountDeviceOperationsByUser.apply(
				u, devHistoryRepo.findAll(), GwId, DeviceId, from, to).get();
		
		assertEquals(2, nb);

	}

	@Test
	public void testFindTopMostActiveDevice() {

		when(devHistoryRepo.findAll()).thenReturn(histories);
		when(devRepo.findAll()).thenReturn(devices);
		
		List<DeviceDTO> devices = StatisticFunctional.FnFindTopMostActiveDevices.apply(
				devRepo.findAll(), devHistoryRepo.findAll(), 1);
		
		assertNotNull(devices);
		assertEquals(1, devices.size());
	}
}
