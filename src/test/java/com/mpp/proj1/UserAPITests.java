package com.mpp.proj1;

import static org.hamcrest.CoreMatchers.any;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.mpp.proj1.api.UserAPI;
import com.mpp.proj1.dto.UserDTO;
import com.mpp.proj1.dto.UserInputDTO;
import com.mpp.proj1.model.Gateway;
import com.mpp.proj1.model.User;
import com.mpp.proj1.service.UserService;
import com.mpp.proj1.util.JwtTokenUtil;

@RunWith(MockitoJUnitRunner.class)
public class UserAPITests {
	
	@Mock
	UserService userService;
	@Mock
	JwtTokenUtil jwt;
	
	@InjectMocks
	UserAPI userAPI;
	
	private User userTests = new User(1, "datpx", "123123", "dat", 0, null, null, null, new Gateway());

	@Test
	public void testCheckAccount() {
		
		ResponseEntity checkAccountNullParams = userAPI.checkAccount(null);
		assertEquals(400, checkAccountNullParams.getStatusCodeValue());
		
		when(userService.login(Mockito.any( UserInputDTO.class ))).thenReturn(new UserDTO(new User(1, "datpx", "123123", "dat", 0, null, null, null, new Gateway())));
		when(jwt.generateToken(anyString())).thenReturn("token");
		ResponseEntity checkAccountResponse = userAPI.checkAccount(new UserInputDTO("datpx", "123123", null, null, 0));
		assertEquals(200, checkAccountResponse.getStatusCodeValue());
		
		when(userService.login(Mockito.any( UserInputDTO.class ))).thenReturn(null);
		ResponseEntity checkAccountNull = userAPI.checkAccount(new UserInputDTO("datpx", "123123", null, null, 0));
		assertEquals(404, checkAccountNull.getStatusCodeValue());
	}
	
	@Test
	public void testGetAllUsers() {
		List<UserDTO> listUser = new ArrayList<UserDTO>();
		when(userService.findAll()).thenReturn(listUser);
		List<UserDTO> listUserTestNull = userAPI.getAllUsers();
		assertTrue(listUserTestNull.isEmpty());
		
		listUser.add(new UserDTO(new User(1, "datpx", "123123", "dat", 0, null, null, null, new Gateway())));
		when(userService.findAll()).thenReturn(listUser);
		List<UserDTO> listUserTest = userAPI.getAllUsers();
		assertEquals("datpx", listUserTest.get(0).getUsername());
	}
	
	@Test
	public void testAddUser() {
		
		when(userService.getUserFromUserName(anyString())).thenReturn(new UserDTO(userTests));
		ResponseEntity userAddTestExists = userAPI.addUser(new UserInputDTO("datpx", "123123", null, null, 0));
		assertEquals(400, userAddTestExists.getStatusCodeValue());
		
		when(userService.getUserFromUserName(anyString())).thenReturn(null);
		when(userService.create(Mockito.any( UserInputDTO.class ))).thenReturn(null);
		ResponseEntity userAddTestNull = userAPI.addUser(new UserInputDTO("datpx", "123123", null, null, 0));
		assertEquals(400, userAddTestNull.getStatusCodeValue());
		
		when(userService.create(Mockito.any( UserInputDTO.class ))).thenReturn(new UserDTO(userTests));
		ResponseEntity userAddTest = userAPI.addUser(new UserInputDTO("datpx", "123123", null, null, 0));
		assertEquals(200, userAddTest.getStatusCodeValue());
		
	}

	@Test
	public void testGetUserInfo() {
		
		when(userService.findByUsername(anyString())).thenReturn(null);
		UserDTO userAddTestNull = userAPI.getUserInfo("userName");
		assertNull(userAddTestNull);
		
		when(userService.findByUsername(anyString())).thenReturn(new UserDTO(userTests));
		UserDTO userAddTest = userAPI.getUserInfo("userName");
		assertEquals("datpx", userAddTest.getUsername());
		assertEquals("dat", userAddTest.getName());
	}
	
	public void testUpdateUser() {
		
		when(userService.updateProfile(Mockito.any( UserInputDTO.class ))).thenReturn(null);
		UserDTO userAddTestNull = userAPI.updateUser(new UserInputDTO("datpx", "123123", null, null, 0));
		assertNull(userAddTestNull);
		
		when(userService.updateProfile(Mockito.any( UserInputDTO.class ))).thenReturn(new UserDTO(userTests));
		UserDTO userAddTest = userAPI.updateUser(new UserInputDTO("datpx", "123123", null, null, 0));
		assertEquals("datpx", userAddTest.getUsername());
		assertEquals("dat", userAddTest.getName());
	}
	
	public void testDeleteUser() {
		
		when(userService.findById(anyInt())).thenReturn(null);
		UserDTO userAddTestNull = userAPI.deleteUser(1);
		assertNull(userAddTestNull);
		
		when(userService.findById(anyInt())).thenReturn(new UserDTO(userTests));
		UserDTO userAddTest = userAPI.deleteUser(1);
		assertEquals("datpx", userAddTest.getUsername());
		assertEquals("dat", userAddTest.getName());
	}

}
