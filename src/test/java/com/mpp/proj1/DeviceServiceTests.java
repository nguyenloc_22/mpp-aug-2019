package com.mpp.proj1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.mpp.proj1.dto.DeviceDTO;
import com.mpp.proj1.dto.DeviceInputDTO;
import com.mpp.proj1.dto.GatewayDTO;
import com.mpp.proj1.dto.GatewayInputDTO;
import com.mpp.proj1.model.Device;
import com.mpp.proj1.model.Gateway;
import com.mpp.proj1.model.Switch;
import com.mpp.proj1.repository.DeviceRepository;
import com.mpp.proj1.repository.GatewayRepository;
import com.mpp.proj1.repository.SwitchRepository;
import com.mpp.proj1.service.impl.DeviceServiceImpl;
import com.mpp.proj1.service.impl.GatewayServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class DeviceServiceTests {
	
	@Mock
	DeviceRepository repository;
	
	@InjectMocks
	DeviceServiceImpl deviceService;
	
	private static	Device deviceTest;
	
	@BeforeClass
	public static void before() {
		deviceTest = new Device("device1", "gateway1", "dv1");
	}
	

	@Test
	public void testCreate() {
		
		when(repository.findByDeviceIdAndGatewayId(anyString(), anyString())).thenReturn(deviceTest);
		DeviceDTO deviceTestActualNull = deviceService.create(new DeviceInputDTO("device1", "gateway1", "dv1"));
		assertNull(deviceTestActualNull);
		
		when(repository.findByDeviceIdAndGatewayId(anyString(), anyString())).thenReturn(null);
		when(repository.saveAndFlush(any())).thenReturn(deviceTest);
		DeviceDTO deviceTestActual = deviceService.create(new DeviceInputDTO("device1", "gateway1", "dv1"));
		assertTrue("gateway1".equals(deviceTestActual.getGatewayId()));
		assertTrue("device1".equals(deviceTestActual.getName()));
		
	}
	
	@Test
	public void testUpdate() {
		
		when(repository.findByDeviceIdAndGatewayId(anyString(), anyString())).thenReturn(deviceTest);
		when(repository.saveAndFlush(any())).thenReturn(deviceTest);
		DeviceDTO deviceTestActual = deviceService.update(new DeviceInputDTO("device1", "gateway1", "dv2"));
		assertEquals("dv2", deviceTestActual.getName());
		
		when(repository.findByDeviceIdAndGatewayId(anyString(), anyString())).thenReturn(null);
		DeviceDTO deviceTestActualNull = deviceService.update(new DeviceInputDTO("device1", "gateway1", "dv1"));
		assertNull(deviceTestActualNull);
	}
	
	@Test
	public void tesFindByGatewayId() {
		List<Device> listDevices = new ArrayList<Device>();
		
		List<DeviceDTO> deviceTestActualNull = deviceService.findByGatewayId("gateway1");
		assertTrue(deviceTestActualNull.isEmpty());
		
		listDevices.add(deviceTest);
		when(repository.findAll()).thenReturn(listDevices);
		List<DeviceDTO> deviceTestActual = deviceService.findByGatewayId("gateway1");
		assertEquals("device1", deviceTestActual.get(0).getName());
		
	}
	
//	@Test
//	public void testFindAll() {
//		
//		List<Gateway> gateways = new ArrayList<Gateway>();
//		
//		when(repository.findAll()).thenReturn(gateways);
//		List<GatewayDTO> listGatewayActualNull = gatewayService.findAll();
//		assertTrue(listGatewayActualNull.isEmpty());
//		
//		gateways.add(gatewayTest);
//		gateways.add(gatewayTest);
//		gateways.add(gatewayTest);
//		
//		when(repository.findAll()).thenReturn(gateways);
//		
//		List<GatewayDTO> listGatewayActual = gatewayService.findAll();
//		assertEquals(3, listGatewayActual.size());
//		assertTrue("gw1".equals(listGatewayActual.get(0).getGatewayId()));
//		assertTrue("gateway1".equals(listGatewayActual.get(0).getName()));
//	}
//	
//	@Test
//	public void testUpdateProfile() {
//		
//		when(repository.findByGatewayId(anyString())).thenReturn(null);
//		GatewayDTO gatewayActualNull = gatewayService.update(new GatewayInputDTO("gw1", "gateway1"));
//		assertNull(gatewayActualNull);
//		
//		when(repository.findByGatewayId(anyString())).thenReturn(gatewayTest);
//		Gateway gateway = new Gateway();
//		gateway.setName("gateway2");
//		gateway.setId(1);
//		gateway.setGatewayId("gw2");
//		when(repository.saveAndFlush(any())).thenReturn(gateway);
//		GatewayDTO gatewayActual = gatewayService.update(new GatewayInputDTO("gw2", "gateway2"));
//		assertEquals("gw2", gatewayActual.getGatewayId());
//		assertEquals("gateway2", gatewayActual.getName());
//	}
//	
//	@Test
//	public void testFindByGatewayId() {
//		
//		when(repository.findByGatewayId(anyString())).thenReturn(null);
//		GatewayDTO gatewayActualNull = gatewayService.findByGatewayId("gw1");
//		assertNull(gatewayActualNull);
//		
//		when(repository.findByGatewayId(anyString())).thenReturn(gatewayTest);
//		GatewayDTO gatewayActual = gatewayService.findByGatewayId("gw1");
//		assertEquals("gw1", gatewayActual.getGatewayId());
//		assertEquals("gateway1", gatewayActual.getName());
//	}
//	
//	@Test
//	public void testFindByName() {
//		
//		List<Gateway> gateways = new ArrayList<Gateway>();
//		when(repository.findByName(anyString())).thenReturn(gateways);
//		List<GatewayDTO> listGatewayTestNull = gatewayService.findByName("gatewayname");
//		assertEquals(0, listGatewayTestNull.size());
//		
//		gateways.add(gatewayTest);
//		when(repository.findByName(anyString())).thenReturn(gateways);
//		List<GatewayDTO> listGatewayTest = gatewayService.findByName("userName");
//		assertEquals(1, listGatewayTest.size());
//		assertEquals("gateway1", listGatewayTest.get(0).getName());
//	}
	
	
	
}
