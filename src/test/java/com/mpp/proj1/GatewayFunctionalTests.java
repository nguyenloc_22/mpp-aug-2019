package com.mpp.proj1;

import com.mpp.proj1.dto.GatewayDTO;
import com.mpp.proj1.functional.GatewayFunctional;
import com.mpp.proj1.model.Gateway;
import com.mpp.proj1.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

@RunWith(MockitoJUnitRunner.class)
public class GatewayFunctionalTests {

    User user;
    private List<Gateway> gatewayList;

    @Before
    public void Setup() {

        /*
         USER PATTERN
         */
        User u1 = new User();
        u1.setUserId(1);
        u1.setName("User-1");
        user = u1;

        User u2 = new User();
        u2.setUserId(2);
        u2.setName("User-2");

        User u3 = new User();
        u3.setUserId(3);
        u3.setName("User-3");

        /*
         GATEWAY PATTERN
         */
        Gateway gw1 = new Gateway();
        gw1.setGatewayId("gw-001");
        gw1.setName("Gateway-01");
        gw1.setConnected(false);
        Set<User> userList1 = new HashSet<User>();
        userList1.add(u1);
        userList1.add(u2);
        gw1.setUsers(userList1);

        Gateway gw2 = new Gateway();
        gw2.setGatewayId("gw-002");
        gw2.setName("Gateway-02");
        gw2.setConnected(true);
        Set<User> userList2 = new HashSet<User>();
        userList2.add(u2);
        userList2.add(u3);
        gw2.setUsers(userList2);

        Gateway gw3 = new Gateway();
        gw3.setGatewayId("gw-003");
        gw3.setName("Gateway-03");
        gw3.setConnected(false);
        Set<User> userList3 = new HashSet<User>();
        userList3.add(u3);
        userList3.add(u1);
        gw3.setUsers(userList3);


        Gateway gw4 = new Gateway();
        gw4.setGatewayId("gw-004");
        gw4.setName("Gateway-04");
        gw4.setConnected(true);
        Set<User> userList4 = new HashSet<User>();
        userList4.add(u1);
        userList4.add(u2);
        gw4.setUsers(userList4);

        Gateway gw5 = new Gateway();
        gw5.setGatewayId("gw-005");
        gw5.setName("Gateway-05");
        gw5.setConnected(true);
        Set<User> userList5 = new HashSet<User>();
        userList5.add(u2);
        userList5.add(u3);
        gw2.setUsers(userList5);

        gatewayList = new ArrayList<>();
        gatewayList = Arrays.asList(gw1, gw2, gw3, gw4, gw5);

    }

    @Test
    public void TestFindAllGateways() {
        List<GatewayDTO> listResult = GatewayFunctional.findAllGateways.apply(gatewayList);
        Assert.assertTrue("TestFindAllGateways Fail",
                listResult.size() == 5);
    }

    @Test
    public void TestFindGatewayById() {
        GatewayDTO result = GatewayFunctional.findGatewaysByGatewayId.apply(gatewayList, "gw-003");
        Assert.assertEquals("TestGetGatewayById Fail",
                result.getGatewayId(), "gw-003");
    }

    @Test
    public void TestFindGatewaysByGatewayName() {
        List<GatewayDTO> listResult = GatewayFunctional.findGatewaysByGatewayName.apply(gatewayList, "Gateway-04");
        Assert.assertTrue("TestFindGatewaysByGatewayName Fail",
                listResult.size() == 1);
    }

    @Test
    public void TestFindGatewaysBelongToUser() {
        List<GatewayDTO> listResult = GatewayFunctional.findGatewaysBelongToUser.apply(gatewayList, user);
        Assert.assertTrue("TestFindGatewaysBelongToUser Fail",
                listResult.size() == 3);
    }

    @Test
    public void TestFindGatewaysIsConnecting() {
        List<GatewayDTO> listResult = GatewayFunctional.findGatewaysIsConnecting.apply(gatewayList);
        Assert.assertTrue("TestFindGatewaysIsConnecting Fail",
                listResult.size() == 3);
    }

    @Test
    public void TestFindGatewaysIsNotConnecting() {
        List<GatewayDTO> listResult = GatewayFunctional.findGatewaysIsNotConnecting.apply(gatewayList);
        Assert.assertTrue("TestFindGatewaysIsNotConnecting Fail",
                listResult.size() == 2);
    }

}
