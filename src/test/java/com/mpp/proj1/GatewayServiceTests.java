package com.mpp.proj1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import com.mpp.proj1.model.User;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.mpp.proj1.dto.GatewayDTO;
import com.mpp.proj1.dto.GatewayInputDTO;
import com.mpp.proj1.model.Gateway;
import com.mpp.proj1.model.Switch;
import com.mpp.proj1.repository.GatewayRepository;
import com.mpp.proj1.repository.SwitchRepository;
import com.mpp.proj1.service.impl.GatewayServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class GatewayServiceTests {
	
	@Mock
	GatewayRepository repository;
	
	@Mock
	SwitchRepository switchRepository;
	
	@InjectMocks
	GatewayServiceImpl gatewayService;
	
	private static	Gateway gatewayTest;
	@BeforeClass
	public static void before() {
		gatewayTest = new Gateway();
		gatewayTest.setName("gateway1");
		gatewayTest.setId(1);
		gatewayTest.setGatewayId("gw1");
		gatewayTest.setConnected(true);
		new Switch("Switch 01", gatewayTest.getGatewayId(), "switch-001");
		new Switch("Switch 02", gatewayTest.getGatewayId(), "switch-002");
	}
	
	@Test
	public void testFindAll() {
		
		List<Gateway> gateways = new ArrayList<Gateway>();
		
		when(repository.findAll()).thenReturn(gateways);
		List<GatewayDTO> listGatewayActualNull = gatewayService.findAll();
		assertTrue(listGatewayActualNull.isEmpty());
		
		gateways.add(gatewayTest);
		gateways.add(gatewayTest);
		gateways.add(gatewayTest);
		
		when(repository.findAll()).thenReturn(gateways);
		
		List<GatewayDTO> listGatewayActual = gatewayService.findAll();
		assertEquals(3, listGatewayActual.size());
		assertTrue("gw1".equals(listGatewayActual.get(0).getGatewayId()));
		assertTrue("gateway1".equals(listGatewayActual.get(0).getName()));
	}
	
	@Test
	public void testFindByGatewayId() {
		List<Gateway> listGateway = new ArrayList<Gateway>();
//		when(repository.findByGatewayId(anyString())).thenReturn(null);
		GatewayDTO gatewayActualNull = gatewayService.findByGatewayId("gw1");
		assertNull(gatewayActualNull);

		listGateway.add(gatewayTest);
		when(repository.findAll()).thenReturn(listGateway);
//		when(repository.findByGatewayId(anyString())).thenReturn(gatewayTest);
		GatewayDTO gatewayActual = gatewayService.findByGatewayId("gw1");
		assertEquals(gatewayActual.getGatewayId(), "gw1");
	}
	
	@Test
	public void testFindByName() {

		List<Gateway> listGateways = new ArrayList<Gateway>();
		List<Gateway> gateways = new ArrayList<Gateway>();
//		when(repository.findByName(anyString())).thenReturn(gateways);
		List<GatewayDTO> listGatewayTestNull = gatewayService.findByGatewayName("gateway1");
		assertEquals(0, listGatewayTestNull.size());

		gateways.add(gatewayTest);
		listGateways.add(gatewayTest);
		when(repository.findAll()).thenReturn(listGateways);
//		when(repository.findByName(anyString())).thenReturn(gateways);
		List<GatewayDTO> listGatewayTest = gatewayService.findByGatewayName("gateway1");
		assertEquals(1, listGatewayTest.size());
		assertEquals("gateway1", listGatewayTest.get(0).getName());
	}

	@Test
	public void testFindGatewaysBelongToUser() {
		List<Gateway> gateways = new ArrayList<Gateway>();
		//when(repository.findGatewaysBelongToUser(any())).thenReturn(gateways);
		List<GatewayDTO> listGatewayTestNull = gatewayService.findGatewaysBelongToUser(new User());
		assertEquals(0, listGatewayTestNull.size());

		gateways.add(gatewayTest);
		//when(repository.findGatewaysBelongToUser(any())).thenReturn(gateways);
		User newUser = new User();
		newUser.setUsername("AK");
		//List<GatewayDTO> listGatewayTest = gatewayService.findGatewaysBelongToUser(newUser);
		//assertEquals(0, listGatewayTest.size());
	}

	@Test
	public void testAdd() {
		GatewayDTO gatewayTestActualNull = gatewayService.add(null);
		assertNull(gatewayTestActualNull);

		when(repository.saveAndFlush(any())).thenReturn(gatewayTest);

		GatewayDTO gatewayTestActual = gatewayService.add(new GatewayInputDTO("gw1", "gate1"));
		assertTrue("gw1".equals(gatewayTestActual.getGatewayId()));
		assertTrue("gateway1".equals(gatewayTestActual.getName()));

	}

	@Test
	public void testUpdate() {

		when(repository.findByGatewayId(anyString())).thenReturn(null);
		GatewayDTO gatewayActualNull = gatewayService.update(new GatewayInputDTO("gw1", "gateway1"));
		assertNull(gatewayActualNull);

		when(repository.findByGatewayId(anyString())).thenReturn(gatewayTest);
		Gateway gateway = new Gateway();
		gateway.setName("gateway2");
		gateway.setId(1);
		gateway.setGatewayId("gw2");
		when(repository.saveAndFlush(any())).thenReturn(gateway);
		GatewayDTO gatewayActual = gatewayService.update(new GatewayInputDTO("gw2", "gateway2"));
		assertEquals("gw2", gatewayActual.getGatewayId());
		assertEquals("gateway2", gatewayActual.getName());
	}
}
