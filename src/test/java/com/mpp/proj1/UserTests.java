package com.mpp.proj1;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import com.mpp.proj1.model.Gateway;
import com.mpp.proj1.model.User;

public class UserTests {

	@Test
	public void test() {
		User testUser = new User(1, "datpx", "123123", "dat", 0, "123123123", null, null, new Gateway());
		assertEquals(1, testUser.getUserId());
		assertEquals("datpx", testUser.getUsername());
		assertEquals("123123", testUser.getPassword());
		assertEquals("dat", testUser.getName());
		assertEquals(0, testUser.getRole());
		assertEquals("123123123", testUser.getPhone());
		assertEquals(null, testUser.getModifyDate());
		assertEquals(null, testUser.getCreateDate());
		assertEquals(1, testUser.getGateways().size());
		
		
		testUser = new User();
		testUser.setUserId(2);
		assertEquals(2, testUser.getUserId());
		
		testUser.setUsername("dat1");
		assertEquals("dat1", testUser.getUsername());
		
		testUser.setPassword("123");
		assertEquals("123", testUser.getPassword());
		
		testUser.setName("dat1995");
		assertEquals("dat1995", testUser.getName());
		
		testUser.setRole(1);
		assertEquals(1, testUser.getRole());
		
		testUser.setPhone("321321321");
		assertEquals("321321321", testUser.getPhone());
		
		testUser.prePersist();
		assertNotEquals(null, (testUser.getCreateDate()));
		assertNotEquals(null, (testUser.getModifyDate()));
		
		Date nowDate = new Date();
		testUser.setCreateDate(nowDate);
		testUser.setModifyDate(nowDate);;
		assertEquals(nowDate, testUser.getModifyDate());
		assertEquals(nowDate, testUser.getCreateDate());
		
		Set<Gateway> setGetway = new HashSet<Gateway>();
		Gateway g1 = new Gateway();
		g1.setConnected(true);
		g1.setGatewayId("1");
		g1.setId(1);
		g1.setName("name");
		setGetway.add(g1);
		testUser.setGateways(setGetway);
		assertEquals(1, testUser.getGateways().size());
	}

}
