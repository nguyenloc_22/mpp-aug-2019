package com.mpp.proj1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.mpp.proj1.dto.GatewayDTO;
import com.mpp.proj1.dto.UserDTO;
import com.mpp.proj1.dto.UserInputDTO;
import com.mpp.proj1.model.Gateway;
import com.mpp.proj1.model.User;	
import com.mpp.proj1.repository.UserRepository;
import com.mpp.proj1.service.impl.UserServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTests {
	
	@Mock
	UserRepository repository;
	
	@InjectMocks
	UserServiceImpl userService;
	
	private static	User userTest;
	@BeforeClass
	public static void before() {
		java.util.Date date = new java.util.Date();
		userTest = new User(1, "datpx", "123123", "dat", 0, "123456789",date, date, new Gateway());
	}
	

	@Test
	public void testLogin() {
		
		when(repository.findByUsernameAndPassword(anyString(), anyString())).thenReturn(Optional.of(userTest));
		
		UserDTO userTestActual = userService.login(new UserInputDTO("datpx", "password", null, null, 0));
		assertTrue("datpx".equals(userTestActual.getUsername()));
		assertTrue("123123".equals(userTestActual.getPassword()));
		
		when(repository.findByUsernameAndPassword(anyString(), anyString())).thenReturn(Optional.empty());
		UserDTO userTestActualNull = userService.login(new UserInputDTO("datpx1", "password", null, null, 0));
		assertNull(userTestActualNull);
		
	}
	
	@Test
	public void testFindAll() {
		
		List<User> listUser = new ArrayList<User>();
		
		when(repository.findAll()).thenReturn(listUser);
		List<UserDTO> userTestActualNull = userService.findAll();
		assertTrue(userTestActualNull.isEmpty());
		
		listUser.add(userTest);
		listUser.add(userTest);
		listUser.add(userTest);
		
		when(repository.findAll()).thenReturn(listUser);
		
		List<UserDTO> userTestActual = userService.findAll();
		assertEquals(3, userTestActual.size());
		assertTrue("datpx".equals(userTestActual.get(0).getUsername()));		
	}
	
	@Test
	public void testFindById() {
		
		List<User> listUsers = new ArrayList<User>();
		when(repository.findAll()).thenReturn(listUsers);
		UserDTO userTestActualNull = userService.findById(0);
		assertNull(userTestActualNull);
		
		listUsers.add(userTest);
		when(repository.findAll()).thenReturn(listUsers);
		UserDTO userTestActual = userService.findById(1);
		assertEquals("datpx", userTestActual.getUsername());
		assertEquals("123123", userTestActual.getPassword());		
	}
	
	@Test
	public void testFindByUsername() {
		List<User> listUsers = new ArrayList<User>();
		when(repository.findAll()).thenReturn(listUsers);
		UserDTO userTestActualNull = userService.findByUsername("datpx");
		assertNull(userTestActualNull);
		
		listUsers.add(userTest);
		when(repository.findAll()).thenReturn(listUsers);
		UserDTO userTestActual = userService.findByUsername("datpx");
		assertTrue("datpx".equals(userTestActual.getUsername()));
		assertTrue("123123".equals(userTestActual.getPassword()));		
	}
	
	@Test
	public void testCreate() {
		List<User> listUsers = new ArrayList<User>();
		listUsers.add(userTest);
		when(repository.findAll()).thenReturn(listUsers);
		
		UserDTO userTestActualNull = userService.create(new UserInputDTO("datpx", "123", null, null, 0));
		assertNull(userTestActualNull);
		
		when(repository.saveAndFlush(any())).thenReturn(userTest);
		UserDTO userTestActual = userService.create(new UserInputDTO("dat", "123", null, null, 0));
		assertEquals("datpx", userTestActual.getUsername());
		assertEquals("123123", userTestActual.getPassword());
		assertEquals(0, userTestActual.getRole());	
	}
	
	@Test
	public void testUpdateProfile() {
		
		UserDTO userTestActualNull = userService.updateProfile(new UserInputDTO("dat", "123", null, null, 0));
		assertNull(userTestActualNull);
		
		List<User> listUsers = new ArrayList<User>();
		listUsers.add(userTest);
		when(repository.findAll()).thenReturn(listUsers);
		when(repository.saveAndFlush(any())).thenReturn(new User(2, "datpx", "123123", "dat1", 0, "9876541",new java.util.Date(), new java.util.Date(), new Gateway()));
		UserDTO userTestActual = userService.updateProfile(new UserInputDTO("datpx", "123", null, null, 0));
		assertEquals("9876541", userTestActual.getPhone());
		assertEquals("dat1", userTestActual.getName());
	}
	
	@Test
	public void testChangePassword() {
		
		when(repository.findByUsername(anyString())).thenReturn(null);
		UserDTO userTestActualNull = userService.changePassword(new UserInputDTO("dat", "123", null, null, 0));
		assertNull(userTestActualNull);
		
		when(repository.findByUsername(anyString())).thenReturn(userTest);
		when(repository.saveAndFlush(any())).thenReturn(new User(2, "datpx", "111111", "dat1", 0, "9876541",new java.util.Date(), new java.util.Date(), new Gateway()));
		UserDTO userTestActual = userService.changePassword(new UserInputDTO("dat", "123123", null, null, 0));
		assertEquals("111111", userTestActual.getPassword());
	}
	
	@Test
	public void testGetUserFromUserName() {
		
		when(repository.findByUsername(anyString())).thenReturn(null);
		UserDTO userTestActualNull = userService.getUserFromUserName("userName");
		assertNull(userTestActualNull);
		
		when(repository.findByUsername(anyString())).thenReturn(userTest);
		UserDTO userTestActual = userService.getUserFromUserName("userName");
		assertEquals("datpx", userTestActual.getUsername());
		assertEquals("123123", userTestActual.getPassword());
	}
	
	@Test
	public void testGetGatewayFromUser() {
		
		when(repository.findByUsername(anyString())).thenReturn(null);
		List<GatewayDTO> listGatewayTestNull = userService.getGatewayFromUser("userName");
		assertNull(listGatewayTestNull);
		
		Set<Gateway> gateways = new HashSet<Gateway>();
		gateways.add(new Gateway());
		when(repository.findByUsername(anyString())).thenReturn(userTest);
		List<GatewayDTO> listGatewayTest = userService.getGatewayFromUser("userName");
		assertEquals(1, listGatewayTest.size());
	}
}
