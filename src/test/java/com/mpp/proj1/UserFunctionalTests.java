package com.mpp.proj1;

import com.mpp.proj1.dto.UserDTO;
import com.mpp.proj1.functional.UserFunctional;
import com.mpp.proj1.model.Gateway;
import com.mpp.proj1.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.*;


public class UserFunctionalTests {

    List<Gateway> gateways;
    private List<User> users;

    @Before
    public void Setup() {
        users = new ArrayList<>();
        User u1 = new User();
        u1.setName("user 1");
        u1.setLoginDate(new Date(2018, Calendar.JANUARY, 1));

        User u2 = new User();
        u2.setName("user 2");
        u2.setLoginDate(new Date(2018, Calendar.FEBRUARY, 1));


        User u3 = new User();
        u3.setName("user 3");
        u3.setLoginDate(new Date(2019, Calendar.MARCH, 1));

        User u4 = new User();
        u4.setName("user 4");
        u4.setLoginDate(new Date(2019, Calendar.APRIL, 1));

        users = Arrays.asList(u1, u2, u3, u4);

    }

    @Test
    public void TestLoginUserByTime() {
        List<UserDTO> listResult =
                UserFunctional.LOGIN_USER_BY_TIME.apply(users
                        , new Date(2017, Calendar.DECEMBER, 1)
                        , new Date(2018, Calendar.JANUARY, 30));

//        UserDTO expectedUserDTO = new UserDTO();
//        expectedUserDTO.setName("user 1");
//        expectedUserDTO.setLoginDate(new Date(2018, Calendar.JANUARY, 1));


        Assert.assertTrue(listResult.size() > 0);


    }


}
