package com.mpp.proj1.api;

import com.mpp.proj1.service.DeviceService;
import com.mpp.proj1.service.GatewayService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.mpp.proj1.dto.DeviceDTO;
import com.mpp.proj1.dto.DeviceInputDTO;
import com.mpp.proj1.dto.GatewayDTO;
import com.mpp.proj1.dto.GatewayInputDTO;
import com.mpp.proj1.dto.UserDTO;
import com.mpp.proj1.error.Error;
import com.mpp.proj1.model.Gateway;
import com.mpp.proj1.service.UserService;

@RestController
@RequestMapping("/device")
public class DeviceAPI {

    @Autowired
    private DeviceService deviceService;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity addDevice(@RequestBody DeviceInputDTO deviceInfo) {
        DeviceDTO deviceDTO = deviceService.create(deviceInfo);
        
        if (deviceDTO == null) {
			return new ResponseEntity<>(Error.DEVICE_DUPLICATE, HttpStatus.BAD_REQUEST);
		}
        return new ResponseEntity<>(deviceDTO, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseEntity updateDevice(@RequestBody DeviceInputDTO deviceInfo) {
    	DeviceDTO deviceDTO = deviceService.update(deviceInfo);
        
        if (deviceDTO == null) {
			return new ResponseEntity<>(Error.DEVICE_NOT_FOUND, HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<>(deviceDTO, HttpStatus.OK);
		}
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public ResponseEntity deleteDevice(@RequestBody String deviceId) {
    	List<DeviceDTO> deviceDTO = deviceService.findByDeviceId(deviceId);
        
        for(DeviceDTO device: deviceDTO) {
        	deviceService.delete(device.getDeviceId());
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @RequestMapping(value = "/device-users", method = RequestMethod.POST)
    public ResponseEntity getListUserDeviceId(@RequestBody String deviceId) {
    	return new ResponseEntity<>(deviceService.listUserByDevice(deviceId), HttpStatus.OK);
    }

    @RequestMapping(value = "/device-users-search", method = RequestMethod.POST)
    public ResponseEntity getListUserDeviceIdSearchByName(@RequestBody String deviceId, String username) {
    	return new ResponseEntity<>(deviceService.listUserSearchByName(deviceId, username), HttpStatus.OK);
    }
}