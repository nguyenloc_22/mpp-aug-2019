package com.mpp.proj1.api;

import com.mpp.proj1.dto.DeviceDTO;
import com.mpp.proj1.dto.DeviceHistoryDTO;
import com.mpp.proj1.dto.DeviceHistoryInputDTO;
import com.mpp.proj1.service.DeviceHistoryService;
import com.mpp.proj1.service.StatisticService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.mpp.proj1.error.Error;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/device-history")
public class DeviceHistoryAPI {

    @Autowired
    private DeviceHistoryService deviceHistoryService;
    
    @Autowired
    private StatisticService statisticService;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity addDeviceHistory(@RequestBody DeviceHistoryInputDTO deviceHistoryInfo) {
        DeviceHistoryDTO deviceHistoryDTO = deviceHistoryService.create(deviceHistoryInfo);

        if (deviceHistoryDTO == null) {
            return new ResponseEntity<>(Error.DEVICE_DUPLICATE, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(deviceHistoryDTO, HttpStatus.CREATED);
    }


    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public ResponseEntity<?> listDeviceHistory(@RequestBody DeviceHistoryInputDTO deviceHistoryInfo) {
        List<DeviceHistoryDTO> deviceHistoryDTO = deviceHistoryService.findAllByDeviceIdAndGatewayIdAndCreateDateBetween(
                deviceHistoryInfo.getDeviceId(),
                deviceHistoryInfo.getGatewayId(),
                new Date(),
                new Date()
        );

        if (deviceHistoryDTO.size() == 0) {
            return new ResponseEntity<>(null, HttpStatus.OK);
        }

        return new ResponseEntity<>(deviceHistoryDTO, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/test-count-dev-operations", method = RequestMethod.GET)
    public ResponseEntity<?> testFunctionalApi_CountDeviceOperator(
    		@RequestParam String gatewayId,
    		@RequestParam String deviceId,
    		@RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date from,
    		@RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date to) {
        
    	//System.out.println(String.format("\n\nGatewayId -> %s; deviceId -> %s; from: %s; to: %s", gatewayId, deviceId, from.toString(), to.toString()));
    	
    	int nbOperators = statisticService.countDeviceOperations(gatewayId, deviceId, from, to);
        return new ResponseEntity<>(nbOperators, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/test-find-dev-history", method = RequestMethod.GET)
    public ResponseEntity<?> testFunctionalApi_FindDeviceHistory(
    		@RequestParam String gatewayId,
    		@RequestParam String deviceId,
    		@RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date from,
    		@RequestParam("to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date to) {
        
    	List<DeviceHistoryDTO> histories = statisticService.findDeviceHistory(gatewayId, deviceId, from, to);
        return new ResponseEntity<>(histories, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/test-find-top-active-device", method = RequestMethod.GET)
    public ResponseEntity<?> testFunctionalApi_FindTopMostActiveDevices(
    		@RequestParam int k) {
        
    	List<DeviceDTO> histories = statisticService.findTopMostActiveDevices(k);
        return new ResponseEntity<>(histories, HttpStatus.OK);
    }

}
