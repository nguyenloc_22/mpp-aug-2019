package com.mpp.proj1.api;

import com.mpp.proj1.service.DeviceService;
import com.mpp.proj1.service.GatewayService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.mpp.proj1.dto.DeviceDTO;
import com.mpp.proj1.dto.GatewayDTO;
import com.mpp.proj1.dto.GatewayInputDTO;
import com.mpp.proj1.model.Switch;
import com.mpp.proj1.socket.Message;
import com.mpp.proj1.socket.SocketIO;

@RestController
@RequestMapping("/gateway")
public class GatewayAPI {

    @Autowired
    private GatewayService gatewayService;
    
    @Autowired
    private DeviceService deviceService;

    /**
     * Adding new gateway.
     * 
     * @param gatewayInfo
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<?> addGateway(@RequestBody GatewayInputDTO gatewayInfo) {

        if (gatewayInfo == null) {
        	return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        try {
	        // check gateway exist
	        GatewayDTO gateway = gatewayService.findByGatewayId(gatewayInfo.getGatewayId());
	        if (gateway != null) {
	            return new ResponseEntity<>("GatewayId existed.", HttpStatus.BAD_REQUEST);
	        }
	
	        // add gateway
	        GatewayDTO gatewayDTO = gatewayService.add(gatewayInfo);
	        return new ResponseEntity<>(gatewayDTO, HttpStatus.CREATED);
	        
        } catch (Exception e) {
        	return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseEntity<?> updateGateway(@RequestBody GatewayInputDTO gatewayInfo) {

    	if (gatewayInfo == null) {
        	return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        
    	try {
            // check gateway exist
            GatewayDTO gateway = gatewayService.findByGatewayId(gatewayInfo.getGatewayId());
            if (gateway == null) {
	            return new ResponseEntity<>("Gateway is not exist.", HttpStatus.BAD_REQUEST);
	        }

            // update gateway
            GatewayDTO gatewayDTO = gatewayService.update(gatewayInfo);
            return new ResponseEntity<>(gatewayDTO, HttpStatus.OK);
            
    	} catch (Exception e) {
    		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public ResponseEntity<?> deleteGateway(@RequestBody GatewayInputDTO gatewayInfo) {

    	if (gatewayInfo == null) {
        	return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

    	try {
            // check gateway exist
            GatewayDTO gateway = gatewayService.findByGatewayId(gatewayInfo.getGatewayId());
            if (gateway == null || gateway.getGatewayId() == null) {
            	return new ResponseEntity<>("Gateway does not exist.", HttpStatus.BAD_REQUEST);
            }

            // delete device
            try {
	            List<DeviceDTO> devices = deviceService.findByGatewayId(gateway.getGatewayId());
	            if(devices != null && !devices.isEmpty()) {
	            	for(int i = 0; i < devices.size(); i++) {
	            		DeviceDTO dev = devices.get(i);
	            		deviceService.delete(dev.getDeviceId(), dev.getGatewayId());
	            	}
	            }
            } catch (Exception e) {}
            
            // delete gateway
            gatewayService.delete(gatewayInfo);;
            
            return new ResponseEntity<>(gatewayInfo, HttpStatus.OK);
            
    	} catch (Exception e) {
    		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

    }
    
    @RequestMapping(value="/list", method = RequestMethod.GET)
    @ResponseBody public List<GatewayDTO> getAllGwateways(){
		List<GatewayDTO> gws = gatewayService.findAll();
        return gws;
    }
    
    @RequestMapping(value="/list-devices", method = RequestMethod.GET)
    @ResponseBody public List<DeviceDTO> getDevicesByGateway(@RequestParam String gatewayId){
		
		List<DeviceDTO> devices = deviceService.findByGatewayId(gatewayId);
        return devices;
    }
    
    @RequestMapping(value="/send-switch-cmd", method = RequestMethod.POST)
    @ResponseBody public Message sendSwitchCmd(@RequestBody Message msg){
		
		if(msg != null) {
			System.out.println(msg.toString());
		} else {
			System.err.println("Message not found!");
		}
		
		SocketIO.getInstance().sendSwitchCommand((Switch)msg.getData());
		
        return msg;
    }

}