package com.mpp.proj1.api;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;

import com.mpp.proj1.dto.GatewayDTO;
import com.mpp.proj1.dto.JwtResponse;
import com.mpp.proj1.dto.UserDTO;
import com.mpp.proj1.dto.UserInputDTO;
import com.mpp.proj1.error.Error;
import com.mpp.proj1.model.Role;
import com.mpp.proj1.model.UserGateway;
import com.mpp.proj1.service.GatewayService;
import com.mpp.proj1.service.UserService;
import com.mpp.proj1.util.JwtTokenUtil;

@RestController
@RequestMapping("/user")
public class UserAPI {

	@Autowired
	private UserService userService;
	@Autowired
	private GatewayService gatewayService;
	
	@Autowired
	private JwtTokenUtil jwt;

	@PostMapping(value = "/login", produces = { "application/json" })
    public ResponseEntity checkAccount(@RequestBody UserInputDTO userInfo){
		
		if(userInfo == null || userInfo.getUserName().isEmpty() || userInfo.getPassword().isEmpty()) {
			return new ResponseEntity<>("Invalid username and or password", HttpStatus.BAD_REQUEST);
		}
		
		UserDTO checkUserDTO = userService.login(userInfo);
		if (checkUserDTO == null) { 
			return new ResponseEntity<>("User not found.", HttpStatus.NOT_FOUND);
		} else {
			String token = jwt.generateToken(checkUserDTO.getUsername());
			return ResponseEntity.ok(new JwtResponse(token));
		}
    }
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<UserDTO> getAllUsers(){
		List<UserDTO> users = userService.findAll();
		return users;
    }
	
	@PostMapping("/create")
	public ResponseEntity addUser(@RequestBody UserInputDTO userInfo){
		UserDTO existUser = userService.getUserFromUserName(userInfo.getUserName());
		if(existUser != null) {
			return new ResponseEntity<>(Error.USER_DUPLICATE, HttpStatus.BAD_REQUEST);
		}
		
		userInfo.setRole(Role.User.ordinal());
		UserDTO userDTO = userService.create(userInfo);
		
		if (userDTO == null) {
			return new ResponseEntity<>(Error.ERR_UNKNOWN, HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<>(userDTO, HttpStatus.OK);
    }

	@RequestMapping(value = "/get-info", method = RequestMethod.GET)
	public UserDTO getUserInfo(@RequestParam String userName) {
		try {
			UserDTO userDTO = userService.findByUsername(userName);
			return (userDTO == null) ? null : userDTO;
		}catch (Exception ex) {
			return null;
		}

	}
	
	@PostMapping("/update")
	public UserDTO updateUser(@RequestBody UserInputDTO userInfo){
		UserDTO userDTO = userService.updateProfile(userInfo);
		return (userDTO == null) ? null : userDTO;
    }
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
    public UserDTO deleteUser(@RequestBody int userId) {
        UserDTO userDTO = userService.findById(userId);
        if (userDTO == null) return null;
        userService.delete(userDTO.getUserId());
        return userDTO;
    }

	@RequestMapping(value = "/change-password", method = RequestMethod.POST)
	public UserDTO changeUserPassword(@RequestBody UserInputDTO userInfo) {
		UserDTO userDTO = userService.findByUsername(userInfo.getUserName());
		if (userDTO == null) return null;
		userService.changePassword(userInfo);
		return userDTO;
	}
	
	/**
	 * Get gws by user
	 * @param currentUser
	 * @return
	 */
	@RequestMapping(value = "/gateways", method = RequestMethod.GET)
    public ResponseEntity getGatewayByUser(Principal currentUser) {
		System.out.println(currentUser.getName());
        List<GatewayDTO> gatewayDTO = userService.getGatewayFromUser(currentUser.getName());
        return new ResponseEntity<>(gatewayDTO, HttpStatus.OK);
    }
	
	/**
	 * Get gws by user
	 * @param currentUser
	 * @return
	 */
	@RequestMapping(value = "/user-gateways", method = RequestMethod.GET)
    public ResponseEntity<List<GatewayDTO>> getGatewayByUser1(@RequestParam String username) {
    
		List<GatewayDTO> gatewayDTO = userService.getGatewayFromUser(username);
        return new ResponseEntity<>(gatewayDTO, HttpStatus.OK);
    }
	
	/**
	 * Get gws by user
	 * @param currentUser
	 * @return
	 */
	@RequestMapping(value = "/link-gw", method = RequestMethod.POST)
    public ResponseEntity<Boolean> addGatewayToUser(@RequestBody UserGateway userGateway) {
		System.out.println("Link gw " + userGateway.getGatewayId() +" to user " + userGateway.getUsername());
        
		// Search user
		UserDTO userDto = userService.getUserFromUserName(userGateway.getUsername());
		if(userDto == null) {
			// return error
		}
		
		GatewayDTO gwDto = gatewayService.findByGatewayId(userGateway.getGatewayId());
		if(gwDto == null) {
			//return error;
		}
		
		userService.addGwToUser(userDto, gwDto);
		
		return new ResponseEntity<>(true, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/remove-gw", method = RequestMethod.POST)
    public ResponseEntity<Boolean> removeGatewayToUser(@RequestBody UserGateway userGateway) {
		System.out.println("Remove gw from user " + userGateway.getGatewayId() +" from " + userGateway.getUsername());
        
		// Search user
		UserDTO userDto = userService.getUserFromUserName(userGateway.getUsername());
		if(userDto == null) {
			// return error
		}
		
		GatewayDTO gwDto = gatewayService.findByGatewayId(userGateway.getGatewayId());
		if(gwDto == null) {
			//return error;
		}
		
		userService.removeGwToUser(userDto, gwDto);
		
		return new ResponseEntity<>(true, HttpStatus.OK);
    }
	
}
