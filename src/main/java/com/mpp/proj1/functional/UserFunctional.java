package com.mpp.proj1.functional;

import com.mpp.proj1.dto.UserDTO;
import com.mpp.proj1.model.Gateway;
import com.mpp.proj1.model.User;
import com.mpp.proj1.util.Functional;

import javax.swing.text.html.Option;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

public class UserFunctional {
    public static final Function<User, UserDTO> mapUserToDTO = UserDTO::new;

    public static Function<User, UserDTO> userDTOFromUser = user -> new UserDTO(user);

    public static final Functional.TriFunction<List<User>, Date, Date, List<UserDTO>> LOGIN_USER_BY_TIME = (users, dateFrom, dateTo)
            -> {
        return users.stream()
                .filter(u -> u.getLoginDate().compareTo(dateFrom) > 0 && u.getLoginDate().compareTo(dateTo) < 0)
                .map(mapUserToDTO)
                .collect(Collectors.toList());
    };

    public static final BiFunction<List<User>, Integer, List<UserDTO>> ACTIVE_USERS = (users, activeStatus)
            -> users.stream().filter(u -> u.getActiveUser() == activeStatus).map(mapUserToDTO).collect(Collectors.toList());
    ;

    public static final BiFunction<List<User>, Integer, Optional<User>> GET_USER_BY_ID = (listUser, id) ->
            listUser.stream()
                    .filter(user -> user.getUserId() == id)
                    .findFirst();

    public static final BiFunction<List<User>, String, Optional<User>> GET_USER_BY_USERNAME = (listUser, username) ->
            listUser.stream()
                    .filter(user -> user.getUsername().equals(username))
                    .findFirst();

    public static final BiFunction<User, Gateway, Boolean> CHECK_GATEWAY_EXISTS = (user, gateway) ->
            user.getGateways()
                    .stream()
                    .anyMatch(gateway1 -> gateway1.getGatewayId().equals(gateway.getGatewayId()));

}
