package com.mpp.proj1.functional;
import com.mpp.proj1.dto.GatewayDTO;
import com.mpp.proj1.dto.UserDTO;
import com.mpp.proj1.model.Gateway;
import com.mpp.proj1.model.User;
import com.mpp.proj1.util.Functional;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

public class GatewayFunctional {

    public static final Function<Gateway, GatewayDTO> gateDTOfromGateway =
            (gateway) -> {
                return new GatewayDTO(gateway);
            };

    public static final Function<List<Gateway>, List<GatewayDTO>> findAllGateways =
            (gatewayList) -> {
                return gatewayList.stream()
                        .map(GatewayDTO::new)
                        .collect(Collectors.toList());
            };

    public static final BiFunction<List<Gateway>, String, GatewayDTO>  findGatewaysByGatewayId =
            (gatewayList, gwId) -> {
                return gatewayList.stream()
                        .filter(gw -> gw.getGatewayId().equals(gwId))
                        .findFirst()
                        .map(GatewayDTO::new)
                        .orElse(null);
            };

    public static final BiFunction<List<Gateway>, String, List<GatewayDTO>> findGatewaysByGatewayName =
            (gatewayList, gwName) -> {
                return gatewayList.stream()
                        .filter(gw -> gw.getName().equals(gwName))
                        .map(GatewayDTO::new)
                        .collect(Collectors.toList());
            };

    public static final BiFunction<List<Gateway>, User, List<GatewayDTO>> findGatewaysBelongToUser =
            (gatewayList, user) -> {
                return gatewayList.stream()
                        .filter(
                                gw -> gw.getUsers().stream()
                                        .anyMatch(us -> us.getUserId() == user.getUserId())
                        )
                        .map(GatewayDTO::new)
                        .collect(Collectors.toList());
            };

    public static final Function<List<Gateway>, List<GatewayDTO>> findGatewaysIsConnecting =
            (gatewayList) -> {
                return gatewayList.stream()
                        .filter(gw -> gw.isConnected() == true)
                        .map(GatewayDTO::new)
                        .collect(Collectors.toList());
            };


    public static final Function<List<Gateway>, List<GatewayDTO>> findGatewaysIsNotConnecting =
            (gatewayList) -> {
                return gatewayList.stream()
                        .filter(gw -> gw.isConnected() == false)
                        .map(GatewayDTO::new)
                        .collect(Collectors.toList());
            };

}
