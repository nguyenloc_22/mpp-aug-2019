package com.mpp.proj1.functional;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.mpp.proj1.dto.DeviceDTO;
import com.mpp.proj1.dto.DeviceHistoryDTO;
import com.mpp.proj1.dto.UserDTO;
import com.mpp.proj1.model.Device;
import com.mpp.proj1.model.DeviceHistory;
import com.mpp.proj1.model.User;
import com.mpp.proj1.util.Functional;
import com.mpp.proj1.util.Functional.TriFunction;

public class StatisticFunctional {

	static TriFunction<Date, Date, Date, Boolean> fnCheckInRange = (d, f, t) -> {
		return d.compareTo(f) > 0 && d.compareTo(t) < 0; 
	};
	
	public static final Functional.SixFunction<List<DeviceHistory>, String, String, Date, Date, List<DeviceHistoryDTO>> 
	FnFindDeviceHistory = (src, gwId, devId, from, to)
        -> {
        	Predicate<DeviceHistory> fnDeviceIdAndGwId= (d) -> {
    			return gwId.equals(d.getGatewayId()) 
    					&& devId.equals(d.getDeviceId());
    		};
    		
    		return src.stream()
    		.filter(d -> fnCheckInRange.apply(d.getCreateDate(), from, to))
    		.filter(fnDeviceIdAndGwId)
    		.map(d -> new DeviceHistoryDTO(d))
    		.collect(Collectors.toList());
    };
    
    public static final Functional.SixFunction<List<DeviceHistory>, String, String, Date, Date, Optional<Integer>> 
    FnCountDeviceOperations = (src, gwId, devId, from, to)
		-> {
			Predicate<DeviceHistory> fnDeviceIdAndGwId= (d) -> {
				return gwId.equals(d.getGatewayId()) 
						&& devId.equals(d.getDeviceId());
			};
			
			int counter = (int)src.stream()
			.filter(fnDeviceIdAndGwId)
			.filter(d -> fnCheckInRange.apply(d.getCreateDate(), from, to))
			.count();
			
			return Optional.of(counter);
    };
	
    public static final Functional.SevFunction<User, List<DeviceHistory>, String, String, Date, Date, Optional<Integer>> 
    FnCountDeviceOperationsByUser = (user, src, gwId, devId, from, to)
	-> {
		TriFunction<User, String, String, Boolean> fnUserHasDevice = (u, gId, dId) -> {
			return u.getGateways().stream()
			.flatMap(gw -> gw.getDevices().stream())
			.anyMatch(d1 -> dId.equals(d1.getDeviceId()) && gwId.equals(d1.getGatewayId()));
		};
		
		int counter = (int)src.stream()
		.filter(d -> fnCheckInRange.apply(d.getCreateDate(), from, to))
		.filter(d -> gwId.equals(d.getGatewayId()) && devId.equals(d.getDeviceId()))
		.filter(d -> fnUserHasDevice.apply(user, d.getGatewayId(), d.getDeviceId()))
		.count();
		
		return Optional.of(counter);
	};
	
	public static final TriFunction<List<Device>, List<DeviceHistory>, Integer, List<DeviceDTO>> 
	FnFindTopMostActiveDevices = (devices, src, n)
	-> {
		// Find top k deviceId from Device history which have
		// most operations.
		BiFunction<List<DeviceHistory>, Integer, List<String>> fnFindMostActiveDev = 
		(devs, k) -> {
			return devs.stream()
			.collect(Collectors.groupingBy(DeviceHistory::getDeviceId, Collectors.counting()))
			.entrySet().stream()
			.sorted((e1, e2) -> (int)(e2.getValue() - e1.getValue()))
			.limit(k)
			.map(e -> e.getKey())
			.collect(Collectors.toList());
		};
		
		return devices.stream()
		.filter(d -> fnFindMostActiveDev.apply(src, n).stream()
					.anyMatch(filteredDev -> filteredDev.equals(d.getDeviceId())))
		.limit(n)
		.map(DeviceDTO::new)
		.collect(Collectors.toList());
	};
	
//	public static List<DeviceDTO> findTopMostActiveDevices(
//			List<Device> devices, List<DeviceHistory> src, int n) {
//		
//		// Find top k deviceId from Device history which have
//		// most operations.
//		BiFunction<List<DeviceHistory>, Integer, List<String>> fnFindMostActiveDev = 
//		(devs, k) -> {
//			return devs.stream()
//			.collect(Collectors.groupingBy(DeviceHistory::getDeviceId, Collectors.counting()))
//			.entrySet().stream()
//			.sorted((e1, e2) -> (int)(e2.getValue() - e1.getValue()))
//			.limit(k)
//			.map(e -> e.getKey())
//			.collect(Collectors.toList());
//		};
//		
//		return devices.stream()
//		.filter(d -> fnFindMostActiveDev.apply(src, n).stream()
//					.anyMatch(filteredDev -> filteredDev.equals(d.getDeviceId())))
//		.limit(n)
//		.map(DeviceDTO::new)
//		.collect(Collectors.toList());
//		
//	}

}
