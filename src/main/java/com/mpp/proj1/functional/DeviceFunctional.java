package com.mpp.proj1.functional;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.mpp.proj1.dto.DeviceDTO;
import com.mpp.proj1.dto.GatewayDTO;
import com.mpp.proj1.model.Device;
import com.mpp.proj1.model.Gateway;

public class DeviceFunctional {

    public static Function<Device, DeviceDTO> deviceDTOFromDevice = DeviceDTO::new;

    public static BiFunction<List<Device>, String, List<Device>> GET_DEVICES_BY_GATE_WAY_ID = (devices, gatewayId) ->
            devices.stream()
                    .filter(device -> device.getGatewayId().equals(gatewayId))
                    .collect(Collectors.toList());

    public static BiFunction<List<Device>, String, List<Device>> GET_DEVICES_BY_ID = (devices, id) ->
            devices.stream()
                    .filter(device -> device.getDeviceId().equals(id))
                    .collect(Collectors.toList());
}
