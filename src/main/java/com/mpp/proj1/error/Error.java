package com.mpp.proj1.error;

public class Error {

    public static final String USER_NOT_FOUND = "User not found";
    public static final String GATEWWAY_NOT_FOUND = "Gateway not found";
    public static final String DEVICE_NOT_FOUND = "Device not found";
    public static final String USER_DUPLICATE = "Username is exists";
    public static final String DEVICE_DUPLICATE = "Device is exists in Gateway";
    public static final String ERR_UNKNOWN = "Unknown error.";
    private String errorMessage;

    public Error(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
    	
        this.errorMessage = errorMessage;
    }
}