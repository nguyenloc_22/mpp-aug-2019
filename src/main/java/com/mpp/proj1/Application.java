package com.mpp.proj1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import com.mpp.proj1.socket.ISocket;
import com.mpp.proj1.socket.SocketIO;

@SpringBootApplication
public class Application extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
				
		return application.sources(Application.class);
	}

	public static void main(String[] args) throws Exception {
		
		System.out.println("Application Initializer >> Booting up ...");
		
		ISocket socketServer = SocketIO.getInstance();
		((SocketIO)socketServer).start();

		SpringApplication.run(Application.class, args);
	}

}
