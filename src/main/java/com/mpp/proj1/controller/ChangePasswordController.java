package com.mpp.proj1.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class ChangePasswordController {

    @RequestMapping(value="/change-password", method = RequestMethod.GET)
    public String showLoginPage(ModelMap model){
        //UserService service;
        return "change-password";
    }

}

