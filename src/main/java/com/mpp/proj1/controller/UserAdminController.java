package com.mpp.proj1.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserAdminController {
    @RequestMapping(value="/user-admin", method = RequestMethod.GET)
    public String showLoginPage(ModelMap model){
        return "user-admin";
    }
}
