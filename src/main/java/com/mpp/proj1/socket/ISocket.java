package com.mpp.proj1.socket;

import com.mpp.proj1.model.Device;

public interface ISocket {
	
//	final String HOST = "localhost";
//	final String HOST = "10.10.63.75";
	final int PORT = 9093;
	
	void sendSwitchCommand(Device dev);
	
	/**
	 * 
	 * @param msg
	 * @param target usually, gatewayId
	 */
	void sendMessage(Message msg, String target);
	
	void broadcastMessage(String topic, Message msg);
	
	void addMessageReceivedListener(IMessageReceiveListener listener);
}
