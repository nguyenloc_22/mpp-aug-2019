package com.mpp.proj1.socket;

import java.io.Serializable;
import java.util.UUID;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(using = MessageDeserializer.class)
public class Message implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3191403525994843501L;
	
	private String id;
	private String receiver;
	private String sender;
	private MessageType type;
	private Object data;
	
	public Message(MessageType type) {
		this.id = uuid().toString();
		this.type = type;
	}
	
	public Message(String id, String target, MessageType type, Object data) {
		this.id = id;
		this.receiver = target;
		this.type = type;
		this.data = data;
	}
	
	public MessageType getType() {
		return this.type;
	}
	
	public Object getData() {
		return data;
	}
	
	public void setData(Object data) {
		this.data = data;
	}
	
	public String getReceiver() {
		return receiver;
	}
	
	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}
	
	public String getSender() {
		return sender;
	}
	
	public void setSender(String sender) {
		this.sender = sender;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public static UUID uuid() {
		return UUID.randomUUID();
	}
	
	@Override
	public String toString() {
		String text = "{id: " + id
		+ "}, {target: " + receiver
		+ "}, {type: " + type
		+ "}, { data: " + data.toString();
		
		return text;
	}

}
