package com.mpp.proj1.socket;

public class GwAuth {
	private String gatewayId;
	private String name;
	private String key;
	
	public GwAuth() {
    }
	
	public GwAuth(String gwId, String name, String key) {
		
		super();
		
		this.gatewayId = gwId;
		this.name = name;
		this.key = key;
	}

	public String getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
	@Override
	public String toString() {
		String text = "[GWAuth]["+ gatewayId +"] -> " + name;
		return text;
	}
	
	
}