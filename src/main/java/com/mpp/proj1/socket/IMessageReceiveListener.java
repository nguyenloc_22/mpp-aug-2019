package com.mpp.proj1.socket;

public interface IMessageReceiveListener {

	void onMessageReceived(Message msg);
}
