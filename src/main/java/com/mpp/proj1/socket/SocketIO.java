package com.mpp.proj1.socket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.mpp.proj1.ApplicationContextProvider;
import com.mpp.proj1.dto.DeviceHistoryInputDTO;
import com.mpp.proj1.model.DeviceHistory;
import com.mpp.proj1.model.Switch;
import com.mpp.proj1.service.DeviceHistoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.ConnectListener;
import com.corundumstudio.socketio.listener.DataListener;
import com.corundumstudio.socketio.listener.DisconnectListener;
import com.mpp.proj1.dto.GatewayDTO;
import com.mpp.proj1.dto.GatewayInputDTO;
import com.mpp.proj1.model.Device;
import com.mpp.proj1.model.Gateway;
import com.mpp.proj1.service.GatewayService;
import com.mpp.proj1.service.impl.DeviceHistoryServiceImpl;


public class SocketIO implements ISocket {
	
    @Autowired
    private GatewayService gatewayService;
	@Autowired
	private DeviceHistoryService deviceHistoryService;
    
	private static SocketIO INSTANCE;
	
	private SocketIOServer server;
	
	private boolean isStarted = false;
	private Map<String, SocketIOClient> devMap = new HashMap<String, SocketIOClient>();
	private List<SocketIOClient> lsUnAuthDevs = new ArrayList<SocketIOClient>();
	private List<IMessageReceiveListener> listeners = new ArrayList<IMessageReceiveListener>();
	
	private final Logger log = LoggerFactory.getLogger(getClass());
		
//	public static void main(String[] args) {
//		new SocketIO().runTest();
//		
//	}
//	
//	public void runTest() {
//		new SocketIO().start();
//	}
	
	
	private SocketIO() { 
		
	}
	
	public static SocketIO getInstance() {
		if(INSTANCE == null) {
			INSTANCE = new SocketIO();
		}
		return INSTANCE;
	}
	
	public void start() {
		if(isStarted) {
			return;
		}
		
		log.debug("[SocketIO] Booting ...");
		
		Configuration config = new Configuration();
        //config.setHostname(HOST);
        config.setPort(PORT);
        
        server = new SocketIOServer(config);
        server.addConnectListener(new SocketConnectListener());
        
        server.addDisconnectListener(new SocketDisconnectListener());
        
        server.addEventListener("gw-auth", Message.class, new GwAuthEventListener());
        
        server.addEventListener("message", Message.class, new DataListener<Message>() {
          @Override
          public void onData(SocketIOClient client, Message data, AckRequest ackRequest) {
        	  log.debug("Receive a message -> " + data.toString());
        	  
        	  if(data.getType() == MessageType.REPORT) {
        		  broadcastMessage("message", data);
        		  try {
					  Switch sw = (Switch) data.getData();
					  DeviceHistoryInputDTO dv = new DeviceHistoryInputDTO();
					  dv.setDeviceId(sw.getDeviceId());
					  dv.setGatewayId(sw.getGatewayId());
					  dv.setStatus(sw.getState());
					  getDevHistoryService().create(dv);
				  } catch (Exception ex) {}
        	  }

        	  for(IMessageReceiveListener l : listeners) {
        		  l.onMessageReceived(data);
        	  }
          }
        });
                
        Runtime.getRuntime().addShutdownHook(new Thread() {
    	    public void run() {
    	    	log.info("[socket-io] Stopped!");
    	    	closeAllConnection();
    	    	server.stop();
    	    }
    	 });

        server.start();
        isStarted = true;
	}
	
	private DeviceHistoryService getDevHistoryService() {
		if(deviceHistoryService == null) {
			deviceHistoryService = ApplicationContextProvider.getApplicationContext().getBean(DeviceHistoryServiceImpl.class);	
		}
		
		return deviceHistoryService;
	}

	
	private void closeAllConnection() {
		for (Map.Entry<String, SocketIOClient> entry : devMap.entrySet()) {
			SocketIOClient s = entry.getValue();
			try {
				s.disconnect();	
			} catch(Exception ex) {}
		}
		
		for(SocketIOClient c : lsUnAuthDevs) {
			try {
				c.disconnect();	
			} catch(Exception ex) {}
			
		}
	}
	
	/**
	 * Send an on/off command to a device
	 * @param dev
	 */
	public void sendSwitchCommand(Device dev) {
		Message msg = new Message(MessageType.COMMAND);
		msg.setData(dev);
		
		this.sendMessage(msg, dev.getGatewayId());
	}
	
	@Override
	public void sendMessage(Message msg, String target) {
		
		SocketIOClient client = devMap.get(target);
		if(client == null) {
			log.error("Cannot send message to un-authenticated device");
			return;
		}
		
		String topic = String.format("%s-%s", msg.getType().getValue(), target);
		log.debug("Sending message at topic -> " + topic);
		client.sendEvent(topic, msg);
		
		log.debug("Sending a message > " + msg.toString());
	}
	
	@Override
	public void broadcastMessage(String topic, Message msg) {
		log.debug("Broadcasting a message into topic -> " + topic);
		server.getBroadcastOperations().sendEvent(topic, msg);

	}

	@Override
	public void addMessageReceivedListener(IMessageReceiveListener listener) {
		if(listeners.indexOf(listener) != -1) {
			log.warn("");
			return;
		}
		
	}
	
	private void updateGwConnectionStatus(String gwId) {
//		try {
//			GatewayDTO gwDto = gatewayService.findByGatewayId(gwId);
//			GatewayInputDTO gwUpdate = new GatewayInputDTO(gwId, gwDto.getName());
//			gwUpdate.setConnected(true);
//			gatewayService.update(gwUpdate);
//		} catch(Exception ex) {
//			log.warn("Can't update gw connection status", ex);
//		}		
	}
	
	class GwAuthEventListener implements DataListener<Message> {
			
		@Override
		public void onData(SocketIOClient client, Message data, AckRequest ackSender) throws Exception {
			GwAuth auth = (GwAuth)data.getData();
			devMap.put(auth.getGatewayId(), client);
			
			Message msg = new Message(MessageType.GwConnChanged);
			Gateway gw = new Gateway();
			gw.setGatewayId(auth.getGatewayId());
			gw.setConnected(true);
			msg.setData(gw);
			broadcastMessage("message", msg);
			
			log.info("Gw authenticated -> " +auth.toString());
			
			// Update connection status
			updateGwConnectionStatus(gw.getGatewayId());
		}
	}
	
	class SocketConnectListener implements ConnectListener {

		@Override
		public void onConnect(SocketIOClient client) {
			UUID sId = client.getSessionId();
			lsUnAuthDevs.add(client);
			log.info("New connection -> " + sId);
		}
		
	}
	
	class SocketDisconnectListener implements DisconnectListener {

		@Override
		public void onDisconnect(SocketIOClient client) {
			UUID sId = client.getSessionId();
			log.info("Connection lost -> " + sId);
			
			String gwId = findGwBySocketInfo(sId);
			
			Message msg = new Message(MessageType.GwConnChanged);
			Gateway gw = new Gateway();
			gw.setGatewayId(gwId);
			gw.setConnected(false);
			msg.setData(gw);
			broadcastMessage("message", msg);
			
			devMap.remove(gwId);
			
			// Update connection status
			updateGwConnectionStatus(gwId);
		}
		
		private String findGwBySocketInfo(UUID sId) {
			String found = null;
			for (Map.Entry<String, SocketIOClient> entry : devMap.entrySet()) {
				SocketIOClient s = entry.getValue();
				if(s.getSessionId() == sId) {
					found = entry.getKey();
					break;
				}
			}
			
			return found;
		}
	}
	
}
