package com.mpp.proj1.socket;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.mpp.proj1.model.Device;
import com.mpp.proj1.model.Switch;

public class MessageDeserializer extends StdDeserializer<Message> { 
 
    /**
	 * 
	 */
	private static final long serialVersionUID = -9215747824966908190L;

	public MessageDeserializer() { 
        this(null); 
    } 
 
    public MessageDeserializer(Class<?> vc) { 
        super(vc); 
    }
 
    @Override
    public Message deserialize(JsonParser jp, DeserializationContext ctxt)
      throws IOException {
    	Message m = null;
    	try {
    	
    		JsonNode node = jp.getCodec().readTree(jp);
            String id = node.get("id").asText();
            String receiver = node.get("receiver").asText();
            String sender = node.get("sender").asText();
            String type = node.get("type").asText();
            
            JsonNode dataNode = node.get("data");
            
            m = new Message(MessageType.valueOf(type));
            m.setId(id);
            m.setReceiver(receiver);
            m.setSender(sender);
            
            switch(m.getType()) {
            
            case AUTH:
            	m.setData(toGwAuth(dataNode));
            	break;
            	
            case COMMAND:
            case REPORT:
            	m.setData(toDevice(dataNode));
            	break;
            
    		default:
    			break;
            
            }
            
            System.out.println("Receive a message -> "+ m.toString());
    	} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
        
        
        return m;
    }
    
    private GwAuth toGwAuth(JsonNode node) {
    	
    	if(node == null) return null;
    	
    	String name = node.get("name").asText();
        String gwId = node.get("gatewayId").asText();
        String key = node.get("key").asText();
        
        GwAuth auth = new GwAuth(gwId, name, key);
        
    	return auth;
    }
    
    private Device toDevice(JsonNode node) {
    	
    	if(node == null) return null;
    	
    	String name = node.get("name").asText();
        String gwId = node.get("gatewayId").asText();
        String devId = node.get("deviceId").asText();
        
        boolean state = node.get("state").asBoolean();
        Switch dev = new Switch(name, gwId, devId);
        dev.setState(state);
        
    	return dev;
    }
    
//  id: '0a6a4a9c-a1a5-4763-a446-4ae082001acd',
//	target: '0a6a4a9c-a1a5-4763-a446-4ae082001acd',
//	type: 'REPORT', 
//	data: {
//		name: 'devName',
//		gwId: 'gw-id',
//		devId: 'dev-id'
//	}
}