package com.mpp.proj1.socket;

public enum MessageType {
	AUTH("auth"),
	COMMAND("command"),
	REPORT("report"),
	GwConnChanged("GwConnChanged");
	
	private String name;
	
	private MessageType(String name) {
		this.name = name;
	}
	
	public String getValue() {
		return name;
	}
	
}
