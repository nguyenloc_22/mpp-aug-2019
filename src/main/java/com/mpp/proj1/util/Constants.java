package com.mpp.proj1.util;

public final class Constants {

	public enum Role{
		Admin,
		Enduser
	}
	
	public static final String DATETIME_FORMAT_JSON = "dd/MM/yyyy HH:mm:ss";
    public static final String TIMEZONE_FORMAT_JSON = "GMT-5";
	
	
}
