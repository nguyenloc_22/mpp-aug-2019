package com.mpp.proj1.util;

public final  class Functional {
    @FunctionalInterface
    public static interface  TriFunction<X,Y,Z,R>{
        R apply(X x,Y y,Z z);
    }
    @FunctionalInterface
    public static interface PentFunction<X,Y,Z,W,R>{
        R apply(X x,Y y ,Z z ,W w);
    }
    
    @FunctionalInterface
    public static interface SixFunction<X,Y,Z,M,N,R>{
        R apply(X x,Y y ,Z z ,M m, N n);
    }

    @FunctionalInterface
    public static interface SevFunction<X,Y,Z,M,N,O,R>{
        R apply(X x,Y y ,Z z ,M m, N n, O o);
    }
    
}
