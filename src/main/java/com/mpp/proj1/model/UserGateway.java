package com.mpp.proj1.model;

public class UserGateway {
	private String username;
	private String gatewayId;
	
	public UserGateway() {
		
	}
	
	public UserGateway(String username, String gwId) {
		this.username = username;
		this.gatewayId = gwId;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}
	
	public String getGatewayId() {
		return gatewayId;
	}
}