package com.mpp.proj1.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "device_history")
public class DeviceHistory {

	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	private int id;
	
	@Column(name = "deviceId")
	private String deviceId;
	
	@Column(name="gatewayId")
	private String gatewayId;

	@Column(name="status")
	private boolean status;

	@Column(name = "create_date", nullable = false)
	private Date createDate;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	public String getGatewayId() {
		return gatewayId;
	}
	public void setGatewayId(String gwId) {
		this.gatewayId = gwId;
	}

	public boolean getStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public DeviceHistory() {
	}
	
	public DeviceHistory(String gatewayId, String deviceId, boolean status) {
		this.gatewayId = gatewayId;
		this.deviceId = deviceId;
		this.status = status;
	}
	
	public DeviceHistory(String gatewayId, String deviceId, boolean status, Date createDate) {
		this(gatewayId, deviceId, status);
		this.createDate = createDate;
	}


	@PrePersist
	public void prePersist() {
		Date now = new Date();
		createDate = now;
	}

}
