package com.mpp.proj1.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//import javax.persistence.CascadeType;
import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.EnumType;
//import javax.persistence.Enumerated;
//import javax.persistence.GeneratedValue;
//import javax.persistence.Id;
//import javax.persistence.JoinColumns;
//import javax.persistence.JoinTable;
//import javax.persistence.ManyToMany;
//import javax.persistence.NamedQuery;
//import javax.persistence.PrePersist;
//import javax.persistence.Table;
//import javax.persistence.UniqueConstraint;
//import javax.validation.constraints.NotNull;

@Entity
//@NamedQuery(name = "User.findByUsername", query = "SELECT u FROM User u WHERE LOWER(u.username) = LOWER(?1)")
//@NamedQuery(name = "User.login", query = "SELECT u FROM User u WHERE LOWER(u.userName) = LOWER(?1) AND LOWER(u.password) = LOWER(?2)")
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private int userId;

    @Column(name = "username", unique = true)
    @NotNull
    private String username;

    @Column(name = "password")
    @NotNull
    private String password;

    @Column(name = "name")
    private String name;

    @Column(name = "role")
    @NotNull
    private int role;

    @Column(name = "phone")
    private String phone;

    @Column(name = "modify_date", nullable = false)
    private Date modifyDate;

    @Column(name = "create_date", nullable = false)
    private Date createDate;

	public Date getLoginDate() {
		return loginDate;
	}

	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}

	public Date getLogoutDate() {
		return logoutDate;
	}

	public void setLogoutDate(Date logoutDate) {
		this.logoutDate = logoutDate;
	}

	@Column(name = "active_user")
	private int activeUser = 0;

	public int getActiveUser() {
		return activeUser;
	}

	public void setActiveUser(int activeUser) {
		this.activeUser = activeUser;
	}

	@ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "user_gateway",
            joinColumns = @JoinColumn(name = "userId", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "gatewayId", referencedColumnName = "id"))
    private Set<Gateway> gateways;
	@Column(name = "login_date", nullable = true)
    private Date loginDate;
    private Date logoutDate;

    public User() {

    }

    public User(int userId, @NotNull String username, @NotNull String password, @NotNull String name, @NotNull int role,
                @NotNull String phone, Date modifyDate, Date createDate, Gateway... gateways) {
        super();
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.name = name;
        this.role = role;
        this.phone = phone;
        this.gateways = Stream.of(gateways).collect(Collectors.toSet());
        this.gateways.forEach(x -> x.getUsers().add(this));
    }

    public Set<Gateway> getGateways() {
        return gateways;
    }

    public void setGateways(Set<Gateway> gateways) {
        this.gateways = gateways;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @PrePersist
    public void prePersist() {
        Date now = new Date();
        createDate = now;
        modifyDate = now;
    }

}
	