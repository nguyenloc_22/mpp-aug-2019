package com.mpp.proj1.model;

public enum DeviceType {
	
	SWITCH("switch");
	
	private String name;
	
	private DeviceType(String name) {
		this.name = name;
	}
	
	public String getValue() {
		return name;
	}
	
}
