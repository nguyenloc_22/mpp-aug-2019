package com.mpp.proj1.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "gateway")
public class Gateway {
	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	private int id;
	
	@Column(name = "gatewayId",unique = true)
	private String gatewayId;

	@Column(name = "name")
	@NotNull
	private String name;
	
	@Transient
	private boolean isConnected = false;
	
//	@ManyToOne
//	@JoinColumn(name="gatewayId")
	
//	@OneToMany(cascade = CascadeType.ALL,
//	        mappedBy = "gatewayId", orphanRemoval = true)
	@Transient
	private List<Device> devices;
	
	@ManyToMany(mappedBy = "gateways")
    private Set<User> users = new HashSet<>();
	
	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public String getGatewayId() {
		return gatewayId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public boolean isConnected() {
		return isConnected;
	}
	
	public void setConnected(boolean isConnected) {
		this.isConnected = isConnected;
	}
	
	public List<Device> getDevices() {
		return devices;
	}
	
	public void setDevices(List<Device> devices) {
		this.devices = devices;
	}

}
