package com.mpp.proj1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "device")
@Inheritance(strategy = InheritanceType.JOINED)
public class Device {

	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	private int id;
	
	@Column(name = "deviceId")
	private String deviceId;
	
	@Column(name="type")
	private int type;
	
//	@ManyToOne
//	private Gateway gateway;
	
	@Column(name="gatewayId")
	private String gatewayId;
	
	@Column(name = "name")
	@NotNull
	private String name;

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	public String getGatewayId() {
		return gatewayId;
	}
	
	public void setGatewayId(String gwId) {
		this.gatewayId = gwId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Device() {
	}
	
	public Device(String name, String gatewayId, String deviceId) {
		this.name = name;
		this.gatewayId = gatewayId;
		this.deviceId = deviceId;
	}

	public Device(@NotNull String name) {
		super();
		this.name = name;
	}

}
