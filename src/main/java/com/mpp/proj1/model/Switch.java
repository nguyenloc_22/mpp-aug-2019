package com.mpp.proj1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "dev_switch")
public class Switch extends Device {

	@Column(name="state")
	private boolean state;
	
	public Switch() {
		
	}
	
	public Switch(String name, String gwId, String devId) {
		super(name, gwId, devId);
		
	}
	
	public Switch(String name, String gwId, String devId, boolean state) {
		super(name, gwId, devId);
		
		this.state = state;
	}
	
	public boolean getState() {
		return state;
	}
	
	public void setState(boolean state) {
		this.state = state;
	}
	
	@Override
	public String toString() {
		return String.format("Switch[%s][%s] -> %s", getName(), getDeviceId(), state);
	}
}
