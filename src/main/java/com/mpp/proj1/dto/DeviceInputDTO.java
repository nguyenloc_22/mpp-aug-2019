package com.mpp.proj1.dto;

public class DeviceInputDTO {

	String deviceId;
	String gatewayId;
	String name;
	
	public DeviceInputDTO(String deviceId, String gatewayId, String name) {
		super();
		this.deviceId = deviceId;
		this.gatewayId = gatewayId;
		this.name = name;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getGatewayId() {
		return gatewayId;
	}
	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}
}

