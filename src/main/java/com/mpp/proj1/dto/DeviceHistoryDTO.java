package com.mpp.proj1.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mpp.proj1.model.DeviceHistory;
import com.mpp.proj1.util.Constants;

import java.util.Date;

public class DeviceHistoryDTO {

    private String deviceId;
    private String gatewayId;
    private boolean status;

    //@JsonFormat(pattern = Constants.DATETIME_FORMAT_JSON, timezone = Constants.TIMEZONE_FORMAT_JSON)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ")
    private Date createDate;

    public DeviceHistoryDTO(DeviceHistory deviceHistory) {
        super();
        this.deviceId = deviceHistory.getDeviceId();
        this.gatewayId = deviceHistory.getGatewayId();
        this.status = deviceHistory.getStatus();
        this.createDate = deviceHistory.getCreateDate();
    }

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public Date getCreateDate() {
		return createDate;
	}
	
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	public boolean getStatus() {
		return status;
	}
	
	public void setStatus(boolean status) {
		this.status = status;
	}

}


