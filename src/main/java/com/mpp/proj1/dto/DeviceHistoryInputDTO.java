package com.mpp.proj1.dto;

import com.mpp.proj1.model.DeviceHistory;

public class DeviceHistoryInputDTO {

    String deviceId;
    String gatewayId;
    boolean status;

    public DeviceHistoryInputDTO(String deviceId, String gatewayId, boolean status) {
        super();
        this.deviceId = deviceId;
        this.gatewayId = gatewayId;
        this.status = status;
    }

    public DeviceHistoryInputDTO() {
        super();
    }

    public String getDeviceId() {
        return deviceId;
    }
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getGatewayId() {
        return gatewayId;
    }
    public void setGatewayId(String gatewayId) {
        this.gatewayId = gatewayId;
    }

    public boolean getStatus() {
        return status;
    }
    public void setStatus(boolean status) {
        this.status = status;
    }

}
