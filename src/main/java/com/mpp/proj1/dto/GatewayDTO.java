package com.mpp.proj1.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mpp.proj1.model.Gateway;
import com.mpp.proj1.model.User;
import com.mpp.proj1.util.Constants;

import java.util.Date;
import java.util.List;

public class GatewayDTO {

    private String gatewayId;
    private String name;
    private boolean isConnected;
    private List<DeviceDTO> devices;
    private List<User> users;

    public GatewayDTO() {
        super();
    }

    public GatewayDTO(Gateway gateway) {
        super();
        this.gatewayId = gateway.getGatewayId();
        this.name = gateway.getName();
    }
    
    public List<DeviceDTO> getDevices() {
		return devices;
	}

    public String getGatewayId() {
        return this.gatewayId;
    }

    public void setGatewayId(String gatewayId) {
        this.gatewayId = gatewayId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public boolean isConnected() {
		return isConnected;
	}
    
    public void setConnected(boolean isConnected) {
		this.isConnected = isConnected;
	}

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @JsonFormat(pattern = Constants.DATETIME_FORMAT_JSON, timezone = Constants.TIMEZONE_FORMAT_JSON)
    private Date modifyDate;

    @JsonFormat(pattern = Constants.DATETIME_FORMAT_JSON, timezone = Constants.TIMEZONE_FORMAT_JSON)
    private Date createDate;

}
