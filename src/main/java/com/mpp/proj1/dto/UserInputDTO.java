package com.mpp.proj1.dto;

import javax.validation.constraints.NotBlank;

import com.mpp.proj1.model.Role;

public class UserInputDTO {
	
	String userName;
	String password;
	String name;	
	String phone;	
	int role;
	
	public UserInputDTO(String userName, String password, String name, String phone, int role) {
		super();
		this.userName = userName;
		this.password = password;
		this.name = name;
		this.phone = phone;
		this.role = role;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}




	public void setName(String name) {
		this.name = name;
	}




	public String getPhone() {
		return phone;
	}




	public void setPhone(String phone) {
		this.phone = phone;
	}




	public int getRole() {
		return role;
	}




	public void setRole(int role) {
		this.role = role;
	}
	
}
