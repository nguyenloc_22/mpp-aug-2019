package com.mpp.proj1.dto;

import com.mpp.proj1.model.Device;

public class DeviceDTO {
	
	private String deviceId;

	private String gatewayId;

	private String name;

	public DeviceDTO(Device device) {
		super();
		this.deviceId = device.getDeviceId();
		this.gatewayId = device.getGatewayId();
		this.name = device.getName();
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
