package com.mpp.proj1.dto;

import com.mpp.proj1.model.Gateway;

public class GatewayInputDTO {

    String gatewayId;
    String name;
    boolean isConnected;

    public GatewayInputDTO(String gatewayId, String name) {
        this.gatewayId = gatewayId;
        this.name = name;
    }

    public String getGatewayId() {
        return this.gatewayId;
    }

    public void setGatewayId(String gatewayId) {
        this.gatewayId = gatewayId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public boolean isConnected() {
		return isConnected;
	}
    
    public void setConnected(boolean isConnected) {
		this.isConnected = isConnected;
	}
}
