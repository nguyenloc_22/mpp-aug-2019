package com.mpp.proj1.dto;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mpp.proj1.model.Role;
import com.mpp.proj1.model.User;
import com.mpp.proj1.util.Constants;

public class UserDTO {

	private int userId;

	private String username;

	private String password;

	private String name;

	private String phone;

	private String note;

	private int role;
	
	private Date loginDate;
	
	public UserDTO(Optional<User> user) {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserDTO(User user) {
		super();
		this.userId = user.getUserId();
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.name = user.getName();
		this.phone = user.getPhone();
		this.role = user.getRole();
		this.loginDate = user.getLoginDate();
		this.createDate = user.getCreateDate();
		this.modifyDate = user.getModifyDate();
	}
	
	public String getUsername() {
		return username;
	}

	public void setUserName(String userName) {
		this.username = userName;
	}
	
	public Date getLoginDate() {
		return loginDate;
	}
	
	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

	@JsonFormat(pattern = Constants.DATETIME_FORMAT_JSON, timezone = Constants.TIMEZONE_FORMAT_JSON)
	private Date modifyDate;

	@JsonFormat(pattern = Constants.DATETIME_FORMAT_JSON, timezone = Constants.TIMEZONE_FORMAT_JSON)
	private Date createDate;

}
