package com.mpp.proj1.repository;

import java.util.Optional;

import com.mpp.proj1.model.User;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends BaseRespository<User, Integer>{
	
	Optional<User> findByUsernameAndPassword(String userName, String Password);

	User findByUsername(String userName);
	
	Optional<User> findByPassword(String password);
	
//	List<User> login(@Param("username") String userName,@Param("password") String password);

}
