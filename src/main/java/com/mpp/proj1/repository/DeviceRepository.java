package com.mpp.proj1.repository;

import java.util.List;

import com.mpp.proj1.model.Device;

public interface DeviceRepository extends BaseRespository<Device, Integer> {

	Device findByName(String name);
	Device findByDeviceIdAndGatewayId(String deviceId, String gatewayId);
	Device findByDeviceId(String deviceId);
	List<Device> findByGatewayId(String gatewayId);

}
