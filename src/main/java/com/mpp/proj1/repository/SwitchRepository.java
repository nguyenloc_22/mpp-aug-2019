package com.mpp.proj1.repository;


import com.mpp.proj1.model.Switch;

public interface SwitchRepository extends BaseRespository<Switch, Integer> {

	Switch findByName(String name);
	Switch findByDeviceIdAndGatewayId(String deviceId, String gatewayId);
	Switch findByDeviceId(String deviceId);

}
