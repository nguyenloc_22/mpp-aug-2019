package com.mpp.proj1.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface BaseRespository<T,ID> extends JpaRepository<T,ID>{

}
