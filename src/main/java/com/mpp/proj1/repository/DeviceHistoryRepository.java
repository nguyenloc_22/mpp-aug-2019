package com.mpp.proj1.repository;

import com.mpp.proj1.model.DeviceHistory;

import java.util.Date;
import java.util.List;

public interface DeviceHistoryRepository extends BaseRespository<DeviceHistory, Integer>  {

    List<DeviceHistory> findAllByDeviceId(String deviceId);
    List<DeviceHistory> findAllByGatewayId(String gatewayId);
    List<DeviceHistory> findAllByStatus(boolean status);
    List<DeviceHistory> findAllByCreateDate(Date createDate);
    List<DeviceHistory> findAllByCreateDateBetween(Date startCreateDate, Date endCreateDate);
    List<DeviceHistory> findAllByDeviceIdAndGatewayId(String deviceId, String gatewayId);
    List<DeviceHistory> findAllByDeviceIdAndGatewayIdAndCreateDateBetween(String deviceId, String gatewayId, Date startCreateDate, Date endCreateDate);

}
