package com.mpp.proj1.repository;

import com.mpp.proj1.model.Gateway;
import com.mpp.proj1.model.User;

import java.util.List;

public interface GatewayRepository extends BaseRespository<Gateway,Integer>{

	Gateway findByGatewayId(String gatewayId);
	List<Gateway> findByName(String name);
	//List<Gateway> findGatewaysBelongToUser(User user);

}
