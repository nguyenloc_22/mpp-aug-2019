//package com.mpp.proj1;
//
//import org.springframework.boot.builder.SpringApplicationBuilder;
//import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
//
//import com.mpp.proj1.socket.ISocket;
//import com.mpp.proj1.socket.SocketIO;
//
//import io.netty.channel.unix.Socket;
//
//public class ServletInitializer extends SpringBootServletInitializer {
//
//	@Override
//	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//		
//		System.out.println("Application Initializer >> Booting up ...");
//		
//		ISocket socketServer = SocketIO.getInstance();
//		//((SocketIO)socketServer).start();
//		
//		return application.sources(Application.class);
//	}
//
//}
