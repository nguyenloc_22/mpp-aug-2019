package com.mpp.proj1.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 
 * Created by Duy Thong Phan 610487.
 * 
 * */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
	  @Bean
	    public Docket api() {
	        return new Docket(DocumentationType.SWAGGER_2)
	                .select()
	                .apis(RequestHandlerSelectors.any())
	                .build()
	                .apiInfo(apiInfo());
	    }

	    private ApiInfo apiInfo() {
	        return new ApiInfoBuilder()
	                .title("Bob Team Devices controller")
	                .description("This project is of part of IOT project")
	                .contact(new Contact("Bob Team", "", "dphan@mum.edu"))
	                .version("1.0")
	                .build();
	    }
	

}
