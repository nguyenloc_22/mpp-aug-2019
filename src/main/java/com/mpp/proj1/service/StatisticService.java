package com.mpp.proj1.service;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.mpp.proj1.dto.*;

public interface StatisticService {

	/**
	 * Find device history for a specific device by it's id in a period of time
	 * 
	 * @param deviceId
	 * @param from
	 * @param to
	 * @return List of {@link DeviceHistoryDTO}
	 */
	List<DeviceHistoryDTO> findDeviceHistory(
			String gwId, String devId, Date from, Date to);
	
	/**
	 * Count device operation from a period of time
	 * 
	 * @param deviceId
	 * @param from
	 * @param to
	 * @return
	 */
	int countDeviceOperations(String gwId, String devId, Date from, Date to);
	
	/**
	 * Count device operations by a user in a period of time
	 * 
	 * @param deviceId
	 * @param from
	 * @param to
	 * @return
	 */
	int countDeviceOperationsByUser(
			String username, String gwId, String devId, Date from, Date to);
	
	/**
	 * Find n most active devices
	 * @param n
	 * @return
	 */
	List<DeviceDTO> findTopMostActiveDevices(int n);
	

}
