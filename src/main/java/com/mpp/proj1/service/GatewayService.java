package com.mpp.proj1.service;
import java.util.List;

import com.mpp.proj1.dto.GatewayDTO;
import com.mpp.proj1.dto.GatewayInputDTO;
import com.mpp.proj1.model.Gateway;
import com.mpp.proj1.model.User;

public interface GatewayService {

    List<GatewayDTO> findAll();

    GatewayDTO findByGatewayId(String gatewayId);

    List<GatewayDTO> findByGatewayName(String gatewayName);

    List<GatewayDTO> findGatewaysBelongToUser(User user);

    List<GatewayDTO> findGatewaysIsConnecting();

    List<GatewayDTO> findGatewaysIsNotConnecting();

    GatewayDTO add(GatewayInputDTO gatewayInputDTO);

    GatewayDTO update(GatewayInputDTO gatewayInputDTO);

    void delete(GatewayInputDTO gatewayInputDTO);

    Gateway convertToEntity(GatewayInputDTO gatewayDTO);

}
