package com.mpp.proj1.service;

import com.mpp.proj1.dto.DeviceHistoryDTO;
import com.mpp.proj1.dto.DeviceHistoryInputDTO;
import com.mpp.proj1.model.DeviceHistory;

import java.util.Date;
import java.util.List;

public interface DeviceHistoryService {

    // CRUD
    DeviceHistoryDTO create(DeviceHistoryInputDTO deviceHistoryInputDTO);

    // Query DeviceHistoryDTO
    List<DeviceHistoryDTO> findAllByDeviceIdAndGatewayIdAndCreateDateBetween(String deviceId, String gatewayId, Date startCreateDate, Date endCreateDate);

    // Others
    DeviceHistory convertToEntity(DeviceHistoryInputDTO deviceHistoryInputDTO);

}
