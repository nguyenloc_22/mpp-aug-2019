package com.mpp.proj1.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.mpp.proj1.dto.GatewayDTO;
import com.mpp.proj1.dto.UserDTO;
import com.mpp.proj1.dto.UserInputDTO;
import com.mpp.proj1.model.Gateway;
import com.mpp.proj1.model.User;
import com.mpp.proj1.repository.GatewayRepository;
import com.mpp.proj1.repository.UserRepository;
import com.mpp.proj1.service.UserService;

import static com.mpp.proj1.functional.UserFunctional.*;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {

	@Autowired
	private UserRepository repository;
	
	@Autowired
	private GatewayRepository gwRepository;

	@Override
	public UserDTO login(UserInputDTO userInputDTO) {
		Optional<User> user = repository.findByUsernameAndPassword(userInputDTO.getUserName(), userInputDTO.getPassword());

		return user.map(mapUserToDTO).orElse(null);
	}

	@Override
	public List<UserDTO> findAll() {
		List<User> users = repository.findAll();
		if (users.size() <= 0) {
			return new ArrayList<>();
		}
		// Java 8
		return users.stream().map(UserDTO::new).collect(Collectors.toList());
	}

	@Override
	public UserDTO findById(int id) {
		List<User> users = repository.findAll();

		Optional<User> user = GET_USER_BY_ID.apply(users, id);

		return user.map(mapUserToDTO).orElse(null);
	}

	@Override
	public UserDTO findByUsername(String username) {
		List<User> users = repository.findAll();

		Optional<User> user = GET_USER_BY_USERNAME.apply(users, username);

		return user.map(mapUserToDTO).orElse(null);
	}

	@Override
	public UserDTO create(UserInputDTO userInputDTO) {
        List<User> users = repository.findAll();
		Optional<User> checkUser = GET_USER_BY_USERNAME.apply(users, userInputDTO.getUserName());

		if (checkUser.isPresent()) {
			return null;
		} 

		User user = repository.saveAndFlush(convertToEntity(userInputDTO));
		return new UserDTO(user);
	}

	@Override
	public UserDTO updateProfile(UserInputDTO userInputDTO) {
        List<User> users = repository.findAll();
		Optional<User> user = GET_USER_BY_USERNAME.apply(users, userInputDTO.getUserName());

		if (!user.isPresent()) {
			return null;
		}
		user.get().setName(userInputDTO.getName());
        user.get().setPhone(userInputDTO.getPhone());
		return new UserDTO(repository.saveAndFlush(user.get()));
	}

	@Override
	public UserDTO changePassword(UserInputDTO userInputDTO) {
		User user = repository.findByUsername(userInputDTO.getUserName());
		if (user == null) return null;
		user.setPassword(userInputDTO.getPassword());
		return new UserDTO(repository.saveAndFlush(user));
	}

	@Override
	public void logout() {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(int userId) {
		repository.deleteById(userId);
	}

	@Override
	public User convertToEntity(UserInputDTO userDTO) {
		User user = new User();
		user.setUsername(userDTO.getUserName());
		user.setPassword(userDTO.getPassword());
		user.setName(userDTO.getName());
		user.setPhone(userDTO.getPhone());
		return user;
	}
	
	public UserDTO getUserFromUserName(String userName) {
		User user = repository.findByUsername(userName);

		if (user != null)
			return new UserDTO(user);
		
		return null;
	}

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		User checkUser = repository.findByUsername(userName);
		
		if (checkUser == null) {
			return null;
		} 
		return new org.springframework.security.core.userdetails.User(checkUser.getUsername(), checkUser.getPassword(), Collections.emptyList());
	}

//	@Override
//	public List<User> findUserByUserNameAndPassword(String userName, String password) {
//		// TODO Auto-generated method stub
//		return null;
//	}
	
	public List<GatewayDTO> getGatewayFromUser(String username) {
		User user = repository.findByUsername(username);
		
		if (user == null) {
			return null;
		}
		List<GatewayDTO> gateways = new ArrayList<GatewayDTO>();
		for(Gateway g: user.getGateways()) {
			gateways.add(new GatewayDTO(g));
		}
		
		return gateways;
	}
	
	public void addGwToUser(UserDTO userDto, GatewayDTO gwDto) {
		User user = repository.findByUsername(userDto.getUsername());
		Gateway gate = gwRepository.findByGatewayId(gwDto.getGatewayId());
		
		if(!checkUserGWExist(user, gate)) {
			user.getGateways().add(gate);
			repository.save(user);	
		}

	}
	
	private boolean checkUserGWExist(User user, Gateway gw) {
		
//		Iterator<Gateway> gws = user.getGateways().iterator();
//		while(gws.hasNext()) {
//			Gateway temp = gws.next();
//			if(gw.getGatewayId().equals(temp.getGatewayId())) {
//				return true;
//			}
//		}
		
		return CHECK_GATEWAY_EXISTS.apply(user, gw);
	}
	
	public void removeGwToUser(UserDTO userDto, GatewayDTO gwDto) {
		User user = repository.findByUsername(userDto.getUsername());
		Gateway gate = gwRepository.findByGatewayId(gwDto.getGatewayId());
		
		user.getGateways().remove(gate);
		repository.save(user);
	}
}
