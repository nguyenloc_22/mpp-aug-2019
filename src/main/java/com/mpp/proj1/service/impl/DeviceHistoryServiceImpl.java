package com.mpp.proj1.service.impl;

import com.mpp.proj1.dto.DeviceHistoryDTO;
import com.mpp.proj1.dto.DeviceHistoryInputDTO;
import com.mpp.proj1.model.DeviceHistory;
import com.mpp.proj1.repository.DeviceHistoryRepository;
import com.mpp.proj1.service.DeviceHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class DeviceHistoryServiceImpl implements DeviceHistoryService {

    @Autowired
    private DeviceHistoryRepository repository;

    @Override
    public DeviceHistoryDTO create(DeviceHistoryInputDTO deviceHistoryInputDTO) {
        DeviceHistory deviceHistory = repository.saveAndFlush(convertToEntity(deviceHistoryInputDTO));
        return new DeviceHistoryDTO(deviceHistory);
    }

    @Override
    public List<DeviceHistoryDTO> findAllByDeviceIdAndGatewayIdAndCreateDateBetween(String deviceId, String gatewayId, Date startDate, Date endDate) {
        //List<DeviceHistory> deviceHistory = repository.findAllByDeviceIdAndGatewayIdAndCreateDateBetween(deviceId, gatewayId, startDate, endDate);
    	List<DeviceHistory> deviceHistory = repository.findAllByDeviceIdAndGatewayId(deviceId, gatewayId);
        if (deviceHistory.size() <= 0) {
            return new ArrayList<>();
        }
        // Java 8
        return StreamSupport.stream(deviceHistory.spliterator(), false).map(DeviceHistoryDTO::new).collect(Collectors.toList());
    }

    @Override
    public DeviceHistory convertToEntity(DeviceHistoryInputDTO deviceHistoryInputDTO) {
        DeviceHistory deviceHistory = new DeviceHistory();
        deviceHistory.setDeviceId(deviceHistoryInputDTO.getDeviceId());
        deviceHistory.setGatewayId(deviceHistoryInputDTO.getGatewayId());
        deviceHistory.setStatus(deviceHistoryInputDTO.getStatus());
        return deviceHistory;
    }
}
