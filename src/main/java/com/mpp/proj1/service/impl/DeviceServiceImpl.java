package com.mpp.proj1.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mpp.proj1.dto.DeviceDTO;
import com.mpp.proj1.dto.DeviceInputDTO;
import com.mpp.proj1.dto.GatewayDTO;
import com.mpp.proj1.dto.UserDTO;
import com.mpp.proj1.model.Device;
import com.mpp.proj1.model.Gateway;
import com.mpp.proj1.repository.DeviceRepository;
import com.mpp.proj1.repository.GatewayRepository;
import com.mpp.proj1.service.DeviceService;
import static com.mpp.proj1.functional.DeviceFunctional.*;
import static com.mpp.proj1.functional.GatewayFunctional.*;
import static com.mpp.proj1.functional.UserFunctional.*;
@Service
public class DeviceServiceImpl implements DeviceService {

	@Autowired
	DeviceRepository repository;
	
	@Autowired
	GatewayRepository gatewayRepository;
	
	@Override
	public DeviceDTO create(DeviceInputDTO deviceInfo) {
		Device checkDevice = repository.findByDeviceIdAndGatewayId(deviceInfo.getDeviceId(), deviceInfo.getGatewayId());
		
		if (checkDevice != null) {
			return null;
		} 

		Device device = repository.saveAndFlush(convertToEntity(deviceInfo));
		return new DeviceDTO(device);
	}

	@Override
	public DeviceDTO update(DeviceInputDTO deviceInfo) {
		Device device = repository.findByDeviceIdAndGatewayId(deviceInfo.getDeviceId(), deviceInfo.getGatewayId());

		if (device == null) {
			return null;
		}
		device.setName(deviceInfo.getName());

		return new DeviceDTO(repository.saveAndFlush(device));
	}

	@Override
	public void delete(String deviceId) {
		Device device = repository.findByDeviceId(deviceId);
		if (device != null) {
			repository.deleteById(device.getId());
		}
	}
	
	@Override
	public void delete(String deviceId, String gatewayId) {
		Device device = repository.findByDeviceIdAndGatewayId(deviceId, gatewayId);
		if (device != null) {
			repository.deleteById(device.getId());
		}
	}


	@Override
	public Device convertToEntity(DeviceInputDTO deviceInputDTO) {
		Device device = new Device();
		device.setName(deviceInputDTO.getName());
		return device;
	}

	@Override
	public List<DeviceDTO> findByDeviceId(String deviceId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DeviceDTO> findByGatewayId(String gatewayId) {
		return GET_DEVICES_BY_GATE_WAY_ID.apply(repository.findAll(), gatewayId).stream().map(deviceDTOFromDevice::apply).collect(Collectors.toList());
	}
	
	public List<UserDTO> listUserByDevice(String deviceId) {
		
		List <Gateway> gateways = gatewayRepository.findAll();
		
		return GET_DEVICES_BY_ID.apply(repository.findAll(), deviceId)
				.stream()
				.map(device -> findGatewaysByGatewayId.apply(gateways, device.getDeviceId()))
				.flatMap(gateway -> gateway.getUsers().stream())
				.map(user -> userDTOFromUser.apply(user))
				.collect(Collectors.toList());
	}
	
	public List<UserDTO> listUserSearchByName(String deviceId, String name) {
		return this.listUserByDevice(deviceId)
				.stream()
				.filter(userDTO -> userDTO.getName().equals(name))
				.collect(Collectors.toList());
	}

}
