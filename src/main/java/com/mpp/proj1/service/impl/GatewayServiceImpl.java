package com.mpp.proj1.service.impl;

import com.mpp.proj1.dto.DeviceDTO;
import com.mpp.proj1.dto.GatewayDTO;
import com.mpp.proj1.dto.GatewayInputDTO;
import com.mpp.proj1.functional.GatewayFunctional;
import com.mpp.proj1.model.Device;
import com.mpp.proj1.model.Gateway;
import com.mpp.proj1.model.Switch;
import com.mpp.proj1.model.User;
import com.mpp.proj1.repository.DeviceRepository;
import com.mpp.proj1.repository.GatewayRepository;
import com.mpp.proj1.repository.SwitchRepository;
import com.mpp.proj1.service.GatewayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import static com.mpp.proj1.functional.GatewayFunctional.*;

@Service
public class GatewayServiceImpl implements GatewayService {

    @Autowired
    GatewayRepository gatewayRepository;
    @Autowired
    private DeviceRepository deviceRepository;
    @Autowired
    private SwitchRepository switchRepository;

    @Override
    public List<GatewayDTO> findAll() {
        List<Gateway> gateways = gatewayRepository.findAll();
        return GatewayFunctional.findAllGateways.apply(gateways);
    }

    @Override
    public GatewayDTO findByGatewayId(String gatewayId) {
        List<Gateway> gateways = gatewayRepository.findAll();
        return GatewayFunctional.findGatewaysByGatewayId.apply(gateways, gatewayId);
    }

    @Override
    public List<GatewayDTO> findByGatewayName(String gatewayName) {
        List<Gateway> gateways = gatewayRepository.findAll();
        return GatewayFunctional.findGatewaysByGatewayName.apply(gateways, gatewayName);
    }

    @Override
    public List<GatewayDTO> findGatewaysBelongToUser(User user) {
        List<Gateway> gateways = gatewayRepository.findAll();
        return GatewayFunctional.findGatewaysBelongToUser.apply(gateways, user);
    }

    @Override
    public List<GatewayDTO> findGatewaysIsConnecting() {
        List<Gateway> gateways = gatewayRepository.findAll();
        return GatewayFunctional.findGatewaysIsConnecting.apply(gateways);
    }

    @Override
    public List<GatewayDTO> findGatewaysIsNotConnecting() {
        List<Gateway> gateways = gatewayRepository.findAll();
        return GatewayFunctional.findGatewaysIsNotConnecting.apply(gateways);
    }

    @Override
    public GatewayDTO add(GatewayInputDTO gatewayInputDTO) {

        // create gateway
        if (gatewayInputDTO == null) return null;
        Gateway gateway = gatewayRepository.saveAndFlush(convertToEntity(gatewayInputDTO));

        // hard-code: create 2 devices belong to this gateway
        Switch device1 = new Switch("Switch 01", gateway.getGatewayId(), "switch-001");
        device1.setState(false);
        switchRepository.saveAndFlush(device1);

        Switch device2 = new Switch("Switch 02", gateway.getGatewayId(), "switch-002");
        device2.setState(false);
        switchRepository.saveAndFlush(device2);

        return new GatewayDTO(gateway);
    }

    @Override
    public GatewayDTO update(GatewayInputDTO gatewayInputDTO) {
        Gateway gateway = gatewayRepository.findByGatewayId(gatewayInputDTO.getGatewayId());
        if (gateway == null) return null;
        gateway.setGatewayId(gatewayInputDTO.getGatewayId());
        gateway.setName(gatewayInputDTO.getName());
        return new GatewayDTO(gatewayRepository.saveAndFlush(gateway));
    }

    @Override
    public void delete(GatewayInputDTO gatewayInputDTO) {
        Gateway gateway = gatewayRepository.findByGatewayId(gatewayInputDTO.getGatewayId());
        if (gateway == null) return;
        gatewayRepository.deleteById(gateway.getId());
    }

    @Override
    public Gateway convertToEntity(GatewayInputDTO gatewayDTO) {
        Gateway gateway = new Gateway();
        gateway.setGatewayId(gatewayDTO.getGatewayId());
        gateway.setName(gatewayDTO.getName());
        return gateway;
    }

}
