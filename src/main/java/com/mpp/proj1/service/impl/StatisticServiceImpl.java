package com.mpp.proj1.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mpp.proj1.dto.DeviceDTO;
import com.mpp.proj1.dto.DeviceHistoryDTO;
import com.mpp.proj1.functional.StatisticFunctional;
import com.mpp.proj1.model.Device;
import com.mpp.proj1.model.DeviceHistory;
import com.mpp.proj1.model.User;
import com.mpp.proj1.repository.DeviceHistoryRepository;
import com.mpp.proj1.repository.DeviceRepository;
import com.mpp.proj1.repository.UserRepository;
import com.mpp.proj1.service.StatisticService;

@Service
public class StatisticServiceImpl implements StatisticService {
	
	@Autowired
	private DeviceHistoryRepository devHistoryRepo;
	@Autowired
	private UserRepository userRepo;
	@Autowired
	private DeviceRepository deviceRepo;
		
	@Override
	public List<DeviceHistoryDTO> findDeviceHistory(String gwId, String devId, Date from, Date to) {
		List<DeviceHistory> historis = devHistoryRepo.findAll();
		
		return StatisticFunctional.FnFindDeviceHistory
		.apply(historis, gwId, devId, from, to);
	}

	@Override
	public int countDeviceOperations(String gwId, String devId, Date from, Date to) {
		List<DeviceHistory> historis = devHistoryRepo.findAll();
				
		return StatisticFunctional.FnCountDeviceOperations
				.apply(historis, gwId, devId, from, to).orElse(0);
	}

	@Override
	public int countDeviceOperationsByUser(String username, String gwId, String devId, Date from, Date to) {
		User user = userRepo.findByUsername(username);
		List<DeviceHistory> historis = devHistoryRepo.findAll();
		
		return StatisticFunctional.FnCountDeviceOperationsByUser
				.apply(user, historis, gwId, devId, from, to)
				.orElse(0);
	}

	@Override
	public List<DeviceDTO> findTopMostActiveDevices(int n) {
		List<Device> devices = deviceRepo.findAll();
		List<DeviceHistory> historis = devHistoryRepo.findAll();
		
		return StatisticFunctional.FnFindTopMostActiveDevices
				.apply(devices, historis, n);
	}

}
