package com.mpp.proj1.service;

import java.security.Principal;
import java.util.List;

import com.mpp.proj1.dto.*;
import com.mpp.proj1.model.Gateway;
import com.mpp.proj1.model.User;


public interface UserService {
	
	
	UserDTO login(UserInputDTO userInputDTO);
	
    List<UserDTO> findAll();

    UserDTO findById(int userId);

    UserDTO findByUsername(String username);

    UserDTO create(UserInputDTO userInputDTO);

    UserDTO updateProfile(UserInputDTO userInputDTO);

    UserDTO changePassword(UserInputDTO userInputDTO);
    
    void logout();
//
    void delete(int userId);
//
    User convertToEntity(UserInputDTO userDTO);
   
    UserDTO getUserFromUserName(String userName);

	List<GatewayDTO> getGatewayFromUser(String name);
	
	void addGwToUser(UserDTO userDto, GatewayDTO gwDto);
	
	void removeGwToUser(UserDTO userDto, GatewayDTO gwDto);



}
