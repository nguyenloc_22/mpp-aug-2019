package com.mpp.proj1.service;
import java.util.List;

import com.mpp.proj1.dto.*;
import com.mpp.proj1.model.Device;

public interface DeviceService {
	
	List<DeviceDTO> findByGatewayId(String gatewayId);

	DeviceDTO create(DeviceInputDTO deviceDTO);

	DeviceDTO update(DeviceInputDTO deviceInfo);

    void delete(String deviceId);
    
    void delete(String deviceId, String gatewayId);
    
    Device convertToEntity(DeviceInputDTO deviceInputDTO);

	List<DeviceDTO> findByDeviceId(String deviceId);
	
	List<UserDTO> listUserByDevice(String deviceId);
	
	List<UserDTO> listUserSearchByName(String deviceId, String name);

}
