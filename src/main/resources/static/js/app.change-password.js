(function() {

    let log = console.log;
    let appUtil = new AppUtil();
    let currentUser;

    function startApp() {
        log('Starting Profile Application!');
        bindEvent();
        loadData();
    }

    function loadData() {
        //TODO: load real user
        loadUserData("huy@gmail.com")
    }

    function bindEvent() {
        $('#btn-change-password').bind('click', onButtonChangePasswordClick);
    }


    function onButtonChangePasswordClick() {

        let oldPassword = $('#txt-old-password').val();
        let newPassword = $('#txt-new-password').val();
        let confirmNewPassword = $('#txt-confirm-new-password').val();

        if (oldPassword !== currentUser.password) {
            toastr.warning('Invalid password.');
            return;
        }

        if (newPassword !== confirmNewPassword) {
            toastr.warning('Password not matched.');
            return;
        }

        appUtil.sendRequest('/user/change-password', {
            userName: currentUser.username,
            password: newPassword,
            // name: name,
            // phone: phone,
            // role: currentUser.role
        }).then(function (value) {
            currentUser.password = newPassword;
            $('#txt-old-password').val("");
            $('#txt-new-password').val("");
            $('#txt-confirm-new-password').val("");
            toastr.success('Successfully changed password.', 'Success');
        })
            .catch(function(err) {
                toastr.warning('Fail to changed password.');
            });
    }



    function loadUserData(username) {
        appUtil.getUserInfo(username)
            .then(function(user) {
                currentUser = user;
                console.log('loaded user', user);
            })
            .catch(function (err) {
                toastr.error('An error occurs while loading page.', 'Error!');
            });

    }

    setTimeout(startApp, 10);

})();