function AppSocket() {
  
  let lsMessageListener = [];
  
  let instance = {
      start: start,
      addMessageListener: addMessageListener,
      removeMessageListener: removeMessageListener
  }
  
  function start() {
    let HOST = AppConstant.Socket.Host;
    let PORT = AppConstant.Socket.Port;
    let origin = window.location.origin;
    let idx = origin.lastIndexOf(':');
    if(idx > 5) { // not in http:
      HOST = origin.substr(0, idx);
    }
    //let socket =  io.connect(HOST);
    let socket =  io.connect(HOST + ':' + PORT);

    socket.on('connect', function() {
      console.log('Connected to server.');
    });

    socket.on('error', function(e) {
      console.log('Error', e);
    });

    socket.on('message', function(data) {
      notifyMessage(data);
    });
  }
  
  function addMessageListener(fn) {
    lsMessageListener.push(fn);
  }
  
  function removeMessageListener(fn) {
    let idx = lsMessageListener.indexOf(fn);
    if(idx !== -1) {
      lsMessageListener.splice(idx, 1);
    }
  }
  
  function notifyMessage(message) {
    for(let i = 0; i < lsMessageListener.length; i++) {
      let fn = lsMessageListener[i];
      fn.call(null, message);
    }
  }
  
  return instance;
  
}