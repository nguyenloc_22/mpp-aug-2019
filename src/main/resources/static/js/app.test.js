(function() {
  
  let log = console.log;
  
  function startApp() {
    log('Starting test applicaiton!');
    
    bindEvent();
  }
  
  function bindEvent() {
    $('#btnSendCmd').bind('click', sendCommandReq);
  }
  
  function sendCommandReq() {
    log('Going to send a command');
    
    let message = {
      id : '123-123-123-123',
      type : 'COMMAND',
      sender : 'user-0001',
      receiver : 'bob-0001',
      data : {
        name : 'bob-gw',
        gwId : 'bob-0001',
        devId : 'dev-0001',
        state : true
      }
    };

    $.post("/test-send-cmd", message, function(data, status) {
      log("Respone -> Data: " + data + "\nStatus: " + status);
    });
    
  }
  
  setTimeout(startApp, 10);
  
})();