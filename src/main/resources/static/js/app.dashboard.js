(function() {
  
  let log = console.log;
  let appUtil = new AppUtil();
  let isAdmin = true;
  
  let jwtToken = '';
  
  function startApp() {
    log('Starting Dashboard applicaiton!');
    
    jwtToken = appUtil.getToken();
    isAdmin = appUtil.isAdmin();
    console.log('Token -> ' + jwtToken);
    console.log('isAdmin -> ' + isAdmin);
    
    // Bind ui events
    bindEvent();
    
    // Load data
    // - Admin -> load all GW
    // - User -> load GWs associated with user
    loadData();
    
    // Start socket client
    let socketClient = new AppSocket();
    socketClient.addMessageListener(onSocketMessage);
    socketClient.start();
    
    
    //toastr.options.preventDuplicates = true;
  }
  
  /**
   * Handle remote event:
   * - Device status report
   * - GW connection status
   */
  function onSocketMessage(message) {
    console.log('Recieve a message -> ' + JSON.stringify(message));
    let msgType = message.type.toLowerCase();
    let dev;
    switch(msgType) {

      case 'report':
        dev = message.data;
        toastr.success(`Device ${dev.deviceId} change: ${dev.state ? 'ON' : 'OFF'}`);
        break;
      
      case 'gwconnchanged':
        /**
          Recieve a message -> 
          {
            "id":"e829e6a3-3089-4acc-b60f-7a120eed9be0",
            "type":"GwConnChanged",
            "data":{
              "id":0,
              "connected":false,
              "gateWayId":"gw-bob-001"
              }
            }
         */
        ui_updateGwConnectionStatus(message.data);
        
        break;
        
      default:
        break;
    }
    
    function ui_updateGwConnectionStatus(gw) {
      if(gw.connected) {
        toastr.success('Gateway ' +gw.gateWayId + ' connected.', 'Connection');
        $('#'+ gw.gateWayId +' .fa-wifi').removeClass('fa-wifi-disconnected')
        $('#'+ gw.gateWayId +' .fa-wifi').addClass('fa-wifi-connected')
      } else {
        toastr.error('Connection to ' +gw.gateWayId + ' lost.', 'Connection');
        $('#'+ gw.gateWayId +' .fa-wifi').removeClass('fa-wifi-connected')
        $('#'+ gw.gateWayId +' .fa-wifi').addClass('fa-wifi-disconnected')
      }
    }
    
//    {
//      "id":"921bd480-d578-11e9-9dc9-934c9a186663",
//      "receiver":"*",
//      "sender":"gw-bob-001",
//      "type":"REPORT",
//      "data": {
//        "id":0,
//        "deviceId":"light-001",
//        "gatewayId":"gw-bob-001",
//        "name":"Bob-001",
//        "state":true
//        }
//    }
  }
  
  function bindEvent() {
    if(!isAdmin) {
      $('.item-admin').remove();
    }
    
    $('#btnSignout').bind('click', function() {
      appUtil.cleanCookie();
      window.location.href = '/';
    });
  }
  
  function loadData() {
    let username = appUtil.getCurrentLoginuser();
    loadGateways(username);
  }
  
  function loadGateways(user) {
    let fn = isAdmin ? 
        appUtil.getAllGateways.bind(null) :
        appUtil.getGWsByUser.bind(null);
    //appUtil.getAllGateways(user)
    fn.call(null, user)
    .then(function(gws) {
      //console.log('List gws result -> ' + JSON.stringify(gws));
      ui_createGwsList(gws);
      
      if(gws.length > 0) {
        let firstGw = gws[0].gatewayId;
        setTimeout(loadDevice.bind(null, firstGw), 1);
      }
    })
    .catch(showError.bind(null, 'Failed to load data. See log for more detail'));
  }
  
  function ui_createGwsList(gws) {
    let lsGw = $('#listGw');
    
    for(let i = 0; i < gws.length; i++) {
      let gw = gws[i];
      
      (function(gateway) {
        let elemHtml = ui_createGwElement(gateway);
        let elem = $(elemHtml);
        lsGw.append(elem);
        $(elem).on('click', ui_onGwSelected);  
      })(gw);
    }
    
    if(gws.length > 0) {
      $('.list-group-item:nth-child(2)').addClass('active');
    }
  }
  
  function ui_createGwElement(gw) {
    return '<a id="'+ gw.gatewayId +'" href="#" class="list-group-item list-group-item-action disabled">'
    + '<i class="fa fa-wifi text-gray fa-wifi-box fa-wifi-disconnected"></i>'
    + gw.name+ '<i class="fa fa-angle-double-right fa-fw pull-right"></i></a>';
  }
  
  function ui_onGwSelected(e) {
    $('.list-group-item.active').removeClass('active');
    $(e.target).addClass('active');
    let gwId = e.target.id;
    
    loadDevice(gwId);
    //e.preventDefault();
  }
  
  function loadDevice(gwId) {
    console.log('Going to load device by gwId -> ' + gwId);
    appUtil.getDevices(gwId)
    .then(function(devices) {
      ui_createDeviceList(devices);
    })
    .catch(console.error);
  }
  
  function ui_createDeviceList(devices) {
    
    let lstDevice = $('#lsDevices');
    lstDevice.empty();
    
    for(let i = 0; i < devices.length; i++) {
      let dev = devices[i];
      ui_createDeviceBlock(dev, lstDevice);
      //console.log('Building ui for device -> ' + JSON.stringify(dev));
    }
  }
  
  function ui_createDeviceBlock(dev, parent) {
    let html = 
      '<div class="col-md-6 bg-light switch-container">'
      +'  <div class="row text-center bg-primary">'
      +'      <div class="col-md-12 border rounded-top">'
      +'          <h6 class="m-3">'+dev.name+'</h6>'
      +'      </div>'
      +'  </div>'
      +'  <div class="row">'
      +'      <div class="col-md-12">'
      +'        <a data-gwId="'+dev.gatewayId+'" data-devId="'+ dev.deviceId +'"'
      +'          id="turnon-'+ dev.deviceId +'"'
      +'          class="btn btn-primary btn-block mt-3" href="#">TURN ON</a>'
      +'      </div>'
      +'  </div>'
      +'  <div class="row">'
      +'      <div class="col-md-12">'
      +'        <a data-gwId="'+dev.gatewayId+'" data-devId="'+ dev.deviceId +'"'
      +'          id="turnoff-'+ dev.deviceId +'"'
      +'          class="btn btn-dark btn-block my-3" href="#">TURN OFF</a></div>'
      +'  </div>'
      +'</div>';
    
    let ele = $(html);
    $(parent).append(ele);
    
    let btnTurnOn = ele.find('#turnon-' + dev.deviceId);
    $(btnTurnOn).on('click', function(e) {
      e.preventDefault();
      sendTurnOnCmd(dev);
    });
    
    let btnTurnOff = ele.find('#turnoff-' + dev.deviceId);
    $(btnTurnOff).on('click', function(e) {
      e.preventDefault();
      sendTurnOffCmd(dev);
    });
    
    function bar() {
      let a = 100;
      
      function foo() {
        let a = 1000;
        console.log(a);
      }
      
      foo();
      
      console.log(a);  
    }
    
    
  }
  
  function sendTurnOnCmd(dev) {
    let message = {
        id: appUtil.uuidv4(),
        type: 'COMMAND',
        sender: 'server',
        receiver: dev.gatewayId,
        data: {
          name: dev.name,
          gatewayId: dev.gatewayId,
          deviceId: dev.deviceId,
          state: true
      }
     };
    
    appUtil.sendRequest('/gateway/send-switch-cmd', message);
    
    toastr.info('Send Turn-On to device ' + dev.name + '('+ dev.gatewayId +')', 'Information!', {
      positionClass: "toast-top-left" 
    });
  }
  
  function sendTurnOffCmd(dev) {
    let message = {
        id: appUtil.uuidv4(),
        type: 'COMMAND',
        sender: 'server',
        receiver: dev.gatewayId,
        data: {
          name: dev.name,
          gatewayId: dev.gatewayId,
          deviceId: dev.deviceId,
          state: false
        }
    };
    
    appUtil.sendRequest('/gateway/send-switch-cmd', message);
    toastr.info('Send Turn-Off to device ' + dev.name + '('+ dev.gatewayId +')', 'Information!', {
      positionClass: "toast-top-left" 
    });
  }
  
  function showError(message, ex) {
    toastr.error('Error', message);
    console.error(ex);
  }
  
  setTimeout(startApp, 10);
  
})();