(function() {
  
  let log = console.log;
  let appUtil = new AppUtil();
  let chart;
  let charts = {};
  let colors = ['blue', 'red', 'green', 'yellow', 'cyan'];

  let allGW = [];
  let selectedGw;
  
  function startApp() {
    console.log('Starting Device Statistic Application.');
    
    bindEvent();
    loadData();
  }
  
  function bindEvent() {
    $('#btnLoadDeviceHistory').bind('click', onBtnLoadDeviceHistoryClick);
  }
  
  function onBtnLoadDeviceHistoryClick() {

    let devs = selectedGw.devices;
    appUtil.getDeviceHistory(devs)
    .then(function(devHistories) {
      console.log('Get devHistories result. ...', devHistories);
      
      let titles = [];
      let chartData = [];
      for (let key in devHistories) {
        if (devHistories.hasOwnProperty(key)) {
           titles.push(findDevNameOfSelectedGw(key));
           
           let dataArr = devHistories[key];
           let ds = [];
           for(let i = 0; i < dataArr.length; i++) {
             
             let d = (typeof dataArr[i].createDate === 'string') ?
                 new Date(dataArr[i].createDate) :
                 dataArr[i].createDate;
             
             ds.push({
               x: d,
               y: (dataArr[i].status === true) ? 1 : 0
             });
           }
           
           chartData.push(ds);
        }
     }

      initChart(titles, chartData);
    })
    .catch(function(err) {
      toastr.error('An error occurs while loading device history', 'Error!');
    });
  }


  let titles = ["Switch 1", "Switch 2"];
  let chartData = [
    [
      { x: "04/01/2014", y: 1 },
      { x: "06/01/2014", y: 0 },
      { x: "07/01/2014", y: 1 },
      { x: "10/01/2014", y: 0 },
      { x: "11/01/2014", y: 1 },
      { x: "13/01/2014", y: 0 },
      { x: "16/01/2014", y: 1 },
      { x: "17/01/2014", y: 0 },
      { x: "21/01/2014", y: 1 },
      { x: "25/01/2014", y: 0 }
    ],
    [
      { x: "04/01/2014", y: 0 },
      { x: "05/01/2014", y: 0 },
      { x: "07/01/2014", y: 1 },
      { x: "10/01/2014", y: 0 },
      { x: "12/01/2014", y: 1 },
      { x: "14/01/2014", y: 0 },
      { x: "16/01/2014", y: 1 },
      { x: "18/01/2014", y: 0 },
      { x: "21/01/2014", y: 1 },
      { x: "24/01/2014", y: 0 }]
  ];


  function loadData() {
    
    let loginUser = appUtil.getCurrentLoginuser();
    loadGateways(loginUser);
    //window.onload = initChart;
    //initChart(titles, chartData);
  }
  
  function loadGateways(user) {
    appUtil.getAllGateways()
    .then(function(gws) {
      allGws = gws;
      let lsGwItem = $('#lsGwsSelection');
      $(lsGwItem).empty();
      for(let i = 0; i < allGws.length; i++) {
        let gwItem = $('<div class="dropdown-item gw-dropdown-item" data-gwid="'+ allGws[i].gatewayId +'">'+ allGws[i].name +'</div>');
        $(lsGwItem).append(gwItem);
        $(gwItem).bind('click', onGwDropboxSelected);
      }
    });
  }
  
  function onGwDropboxSelected(e) {
    let gwId = e.target.attributes['data-gwid'].value;

    selectedGw = findGwById(gwId);
    appUtil.getDevices(gwId)
    .then(function(devs) {
      selectedGw.devices = devs;
      console.log('Load devices by gw -> ' + JSON.stringify(devs));
    });
    
    $('#btnSelectGw').html(gwId);
  }
  
  function findGwById(gwId) {
    let found = {};
    for(let i = 0; i < allGws.length; i++) {
      if(allGws[i].gatewayId === gwId) {
        found = allGws[i];
        break;
      }
    }
    
    return found;
  }
  
  // 
  function findDevNameOfSelectedGw(gwIdDevId) {
    let devices = selectedGw.devices;
    if(!devices) {
      return gwIdDevId;
    }
    
    for(let i = 0 ; i < devices.length; i++) {
      let dev = devices[i];
      let temp = `${dev.gatewayId}-${dev.deviceId}`;
      if(temp === gwIdDevId) {
        return dev.name;
      }
    }
    
    return gwIdDevId;
  }
  
  function initChart(titles, data) {
    var timeFormat = 'DD/MM/YYYY';

    var config = {
        type:    'line',
        data:    {
            datasets: []
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: "Device usage history"
            },
            scales:     {
                xAxes: [{
                    type: "time",
                    time: {
                        format: timeFormat,
                        tooltipFormat: 'll'
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Time'
                    }
                }],
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'State'
                    }
                }]
            }
        }
    };

    for(let i = 0; i < titles.length; i++) {
      let ds = {
        label: titles[i],
        data: data[i],
        steppedLine: true,
        fill: true,
        borderColor: colors[i],
        //steppedLine: true
        
      };
      config.data.datasets.push(ds);
    }


    var ctx = document.getElementById("canvasId").getContext("2d");

    chart = new Chart(ctx, config);

  }

  function addData(chart, data) {
    //chart.data.labels.push(label);

    let nbDataset = chart.data.datasets.length;
    for(let i = 0; i < nbDataset; i++) {

    }
    chart.data.datasets.forEach((dataset) => {
      dataset.data.push(data);
    });
    chart.update();
  }

  setTimeout(startApp, 10);
  
})();
