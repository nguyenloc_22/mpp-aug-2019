(function() {
	
  let appUtil = new AppUtil();
  
	function run() {
	  
	  let currentLogin = appUtil.getCurrentLoginuser();
	  let jwtToken = appUtil.getToken();
    if(currentLogin && currentLogin.length > 0 && jwtToken && jwtToken.length > 0) {
      window.location.href = '/dashboard';
      return;
    }
	  
		bindEvent();
	}
	
	var user;
	var pwd;
	function bindEvent() {
		// jquery to bind click event to button submit / logins
		$("#buttonLogin").click(login);
	}
	
	function login() {
		let user = $('#email').val();
		let pwd = $('#pwd').val();
		if(!user || !pwd) {
		  toastr.error('Invalid username and / or password!');
		  return;
		}
		
		let userInfo = {
				userName: user,
				password: pwd
		}
		
		appUtil.sendRequest('/user/login', userInfo)
		.then(function(resp) {
		  appUtil.setCookie('bob-usr-token', resp.token);
		  appUtil.setCookie('bob-usr-login', user);
		  window.location.href = '/dashboard'; 
		  toastr.success('Login ok.', 'Success');
		})
		.catch(function(err) {
		  let errMsg = 'An error occurs while logging in. ';
		  if(err && err.responseText) {
		    errMsg += err.responseText;
		    toastr.error(errMsg, 'Error!');
		  }
		});
		
//		$.ajax({
//  		  headers: { 
//          'Accept': 'application/json',
//          'Content-Type': 'application/json'
//        },
//			  url: "/user/login",
//			  method: "POST",
//			  data: JSON.stringify(userInfo),
//			  dateType: "json",
//			}).done(function( response ) {
//				
//			  appUtil.setCookie('bob-token', 'Eugene J Rautenbach!');
//			  window.location.href = '/dashboard'; 
//				toastr.success('Login ok.', 'Success');
//				console.log(response);
//			})
//			.fail(function(err) {
//			  toastr.error('Fail to login.', 'Error');
//			  console.log('An error occurs while logging in', err);
//			})
	}
	
	setTimeout(run, 10);
	
})();