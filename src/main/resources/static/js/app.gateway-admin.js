(function() {

    let log = console.log;
    let appUtil = new AppUtil();

    let allGW = [];
    let selectedGateway;

    function startApp() {
        log('Starting Gateway Admin Application!');
        bindEvent();
        loadData();
    }

    function loadData() {
      let loginUser = appUtil.getCurrentLoginuser();
      loadGateways(loginUser);
      
      //console.log('Loading gw by user -> ' + loginUser);
    }

    function bindEvent() {
        $('#btn-add-gateway').bind('click', onButtonAddGatewayClick);
        $('#btn-update-gateway').bind('click', onButtonUpdateGatewayClick);
        $('#btn-delete-gateway').bind('click', onButtonDeleteGatewayClick);
        
        $('#btnSignout').bind('click', function() {
          appUtil.cleanCookie();
          window.location.href = '/';
        });
    }

    /**
     * Handle add gw button.
     * 
     * Name: A human readable text
     * ID: GatewayId must be mapped to the gw's software.
     */
    function onButtonAddGatewayClick() {
        let name = $('#txtGatewayName').val();
        let gatewayId = $('#txtGatewayId').val();
        if(!name || !gatewayId) {
          toastr.error('Please enter name and id.', 'Error!');
          return;
        }

        appUtil.sendRequest('/gateway/add', {
          gatewayId: gatewayId,
          name: name
        }).then(function (createdGw) {
          toastr.success('Success', 'Successfully added new gateway')
          
          $('#txtGatewayName').val("");
          $('#txtGatewayId').val("");

          let elemHtml = ui_createGwElement(createdGw);
          let elem = $(elemHtml);
          $('#listGw').append(elem);
          $(elem).on('click', ui_onGwSelected);
        })
        .catch(function(err) {
            let errMsg = 'Failed to add GW. ';
            if(err && err.responseText) {
              errMsg += err.responseText;
            }
            toastr.warning(errMsg, 'Error!');
        });
    }

    /**
     * Update Gateway's name
     */
    function onButtonUpdateGatewayClick() {
        let gatewayId = selectedGateway.gatewayId;
        let newName = $('#txtUpdatedName').val();
        if(!newName || newName === selectedGateway.name) {
          toastr.error('New name cannot be empty or same.', 'Error!');
          return;
        }
        
        appUtil.sendRequest('/gateway/update', {
          gatewayId: gatewayId,
          name: newName
        }).then(function(updatedGw) {
          $('#txtUpdatedName').val("");
          $('#' + gatewayId + ' .gw-name').html(newName);
          selectedGateway = updatedGw;
          toastr.success('Success', 'Successfully updated new gateway');
        })
        .catch(function(err) {
          let errMsg = 'Fail to update gateway. ';
          if(err && err.responseText) {
            errMsg += err.responseText;
          }
          
          toastr.warning(errMsg, 'Error!');
        });
    }

    /**
     * Delete a GW
     */
    function onButtonDeleteGatewayClick() {
        let gatewayId = selectedGateway.gatewayId;
        
        let confirmed = confirm('Are you sure you want to delete this GW?');
        if(!confirmed) {
          return false;
        }
        
        appUtil.sendRequest('/gateway/delete', {
            gatewayId: gatewayId,
            name: name
        }).then(function (value) {

            let toRemove = $('#' + gatewayId);
            $(toRemove).remove();

            let idx = allGW.indexOf(selectedGateway);
            allGW.splice(idx, 1);
            selectedGateway = null;
            if(allGW.length > 0) {
                selectGateway(allGW[0]);
            }

            if(allGW.length === 0) {
                console.warn("You have deleted all your gateways.");
            } else {
                toastr.success('Success', 'Successfully deleted gateway');
            }

        })
        .catch(function(err) {
          let errMsg = 'An error occurs while deleting gw. ';
          if(err && err.responseText) {
            errMsg += err.responseText;
          }
          toastr.error(errMsg, 'Error!');
        });

    }


    function loadGateways(user) {
        appUtil.getAllGateways(user)
            .then(function(gws) {
                allGW = gws;
                ui_createGwsList(gws);
            })
            .catch(console.error);
    }

    function ui_createGwsList(gws) {
        let lsGw = $('#listGw');

        for(let i = 0; i < gws.length; i++) {
            let gw = gws[i];

            (function(gateway) {
                let elemHtml = ui_createGwElement(gateway);
                let elem = $(elemHtml);
                lsGw.append(elem);
                $(elem).on('click', ui_onGwSelected);
            })(gw);
        }

        if(allGW && allGW.length > 0) {
            $('.list-group-item:nth-child(2)').addClass('active');
            selectedGateway = allGW[0];
        }
    }

    function selectGateway(gw) {
        $('#' + gw.gatewayId).addClass('active');
        selectedGateway = gw;
    }

    function ui_createGwElement(gw) {
        return '<a id="'+ gw.gatewayId +'" href="#" class="list-group-item list-group-item-action disabled">'
            + '<i class="fa fa-wifi text-gray"></i>'
            + '<span class="gw-name">'
            +    gw.name+ '</span><i class="fa fa-angle-double-right fa-fw pull-right"></i></a>';
    }

    function ui_onGwSelected(e) {
        $('.list-group-item.active').removeClass('active');

        selectGateway(findGwById(e.target.id));
        //$(e.target).addClass('active');
    }

    function findGwById(id) {
        let found = null;

        for(let i = 0; i < allGW.length; i++) {
            let gw = allGW[i];
            if(gw.gatewayId === id) {
                found = gw;
                break;
            }
        }

        return found;
    }

    // function onBtnDeleteClicked() {
    //     console.log('Going to delete gw -> ', selectedGateway);
    // }

    setTimeout(startApp, 10);

})();