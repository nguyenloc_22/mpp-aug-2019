(function() {

    let log = console.log;
    let appUtil = new AppUtil();
    let currentUser;

    function startApp() {
        log('Starting Gateway Admin Application!');
        bindEvent();
        loadData();
    }

    function loadData() {
        //TODO: load real user
        loadUserData("huy@gmail.com")
    }

    function bindEvent() {
        $('#btnUpdateProfile').bind('click', onButtonUpdateProfileClick);
        $('#btnChangePassword').bind('click', onButtonChangePasswordClick);
    }


    function onButtonUpdateProfileClick() {

        //let userName = $('#txtEmail').val();
        let name = $('#txtFullName').val();
        let phone = $('#txtPhoneNumber').val();

        appUtil.sendRequest('/user/update', {
            userName: currentUser.username,
            password: currentUser.password,
            name: name,
            phone: phone,
            role: currentUser.role
        }).then(function (value) {
            //currentUser.userName = userName;
            currentUser.name = name;
            currentUser.phone = phone;
            toastr.success('Successfully updated user profile.', 'Success');
        })
            .catch(function(err) {
                toastr.warning('Fail to update user profile.');
            });
    }

    function onButtonChangePasswordClick() {

    }


    function loadUserData(username) {
        appUtil.getUserInfo(username)
            .then(function(user) {
                currentUser = user;
                $('#txtEmail').val(user.username);
                $('#txtFullName').val(user.name);
                $('#txtPhoneNumber').val(user.phone);
            })
            .catch(function (err) {
                toastr.error('An error occurs while loading page.', 'Error!');
            });

    }

    setTimeout(startApp, 10);

})();