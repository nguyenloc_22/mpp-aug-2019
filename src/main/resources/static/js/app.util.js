function AppUtil() {
  
  // ++ Test data
//  let DumpDb = {};
//  let allGw = [
//    {  id: 1, gatewayId: 'gw-bob-001', name: 'Bob 01' },
//    {  id: 2, gatewayId: 'gw-bob-002', name: 'Bob 02' },
//    {  id: 3, gatewayId: 'gw-bob-003', name: 'Bob 03' }
//  ];
//  let allDev = [
//    { id: 1, deviceId: 'switch-001', type: 'switch', gatewayId: 'gw-bob-001', name: 'Study Lamp' },
//    { id: 2, deviceId: 'switch-002', type: 'switch', gatewayId: 'gw-bob-001', name: 'Sleep Lamp' },
//    { id: 3, deviceId: 'switch-003', type: 'switch', gatewayId: 'gw-bob-002', name: 'Fan 1' },
//    { id: 4, deviceId: 'switch-004', type: 'switch', gatewayId: 'gw-bob-002', name: 'Light 1' },
//  ]; 
//  DumpDb['gateways'] = allGw;
//  DumpDb['devices'] = allDev;
  // -- End of test
  
  let service = {
      RequestType: {
        Post: 'POST',
        Get: 'GET'
      },
      getAllGateways: getAllGateways,
      getGWsByUser: getGWsByUser,
      getDevices: getDevices,
      getAllUsers: getAllUsers,
      addGwToUser: addGwToUser,
      removeGwFromUser: removeGwFromUser,
      getUserInfo: getUserInfo,
      getDeviceHistory: getDeviceHistory,
      sendRequest: sendRequest,
      uuidv4: uuidv4,
      setCookie: setCookie,
      getCookie: getCookie,
      getCurrentLoginuser: getCurrentLoginuser,
      isAdmin: isAdmin,
      getToken: getToken,
      cleanCookie: cleanCookie,
      emailIsValid: emailIsValid
  };

  function getDeviceHistory(devs) {

    let rs = {};
    let promise = new Promise((resolve, reject) => {
      internal_getDeviceHistory(devs, 0, rs, resolve);
    });

    return promise;
  }

  function generateDemoData(gatewayId, deviceId) {
    let d = new Date('2019-03-10');
    let t = d.getTime();
    let deltal = 5*60*1000;
    let data = [];

    data.push({ gatewayId: gatewayId, deviceId: deviceId, status: true, time: new Date(t+=deltal) });
    data.push({ gatewayId: gatewayId, deviceId: deviceId, status: false, time: new Date(t+=deltal) });
    data.push({ gatewayId: gatewayId, deviceId: deviceId, status: true, time: new Date(t+=deltal) });
    data.push({ gatewayId: gatewayId, deviceId: deviceId, status: false, time: new Date(t+=deltal) });
    data.push({ gatewayId: gatewayId, deviceId: deviceId, status: true, time: new Date(t+=deltal) });
    data.push({ gatewayId: gatewayId, deviceId: deviceId, status: false, time: new Date(t+=deltal) });
    data.push({ gatewayId: gatewayId, deviceId: deviceId, status: true, time: new Date(t+=deltal) });
    data.push({ gatewayId: gatewayId, deviceId: deviceId, status: false, time: new Date(t+=deltal) });
    data.push({ gatewayId: gatewayId, deviceId: deviceId, status: true, time: new Date(t+=deltal) });
    data.push({ gatewayId: gatewayId, deviceId: deviceId, status: false, time: new Date(t+=deltal) });

    return data;
  }

  function internal_getDeviceHistory(devs, idx, rs, onFinished) {
    if(idx === devs.length) {
      onFinished(rs);
      return;
    }

    let dev = devs[idx];
    idx++;


//    let promise = new Promise(function (resolve, reject) {
//      setTimeout(function() {
//        resolve(generateDemoData(dev.gatewayId, dev.deviceId));
//      })
//    });
//
//    promise.then(function(resp) {
//      let key = `${dev.gatewayId}-${dev.deviceId}`;
//      rs[key] = resp;
//      internal_getDeviceHistory(devs, idx, rs, onFinished);
//    });

    let url = 'device-history/list';
    sendRequest(url, {
      gatewayId: dev.gatewayId,
      deviceId: dev.deviceId
    })
    .then(function(resp) {
      let key = `${dev.gatewayId}-${dev.deviceId}`;
      rs[key] = resp;
      internal_getDeviceHistory(devs, idx, rs, onFinished);
    });

  }
  
  function getUserInfo(username) {
    return sendRequest('user/get-info?userName=' + username, null, 'GET');
  }
  
  function addGwToUser(gwId, username) {
    
    return sendRequest('/user/link-gw', {
      username: username,
      gatewayId: gwId
    });
  }
  
  function removeGwFromUser(username, gwId) {
    return sendRequest('/user/remove-gw', {
      username: username,
      gatewayId: gwId
    });
  }
  
  /**
   * @eeturn Promise resolve all gw
   */
  function getAllGateways() {
    return sendRequest('/gateway/list', null, 'GET');
  }
  
  function getGWsByUser(username) {
    return sendRequest('/user/user-gateways?username=' + username, null, 'GET');
  }
  
  function getDevices(gwId) {
    return sendRequest('/gateway/list-devices?gatewayId=' + gwId, null, 'GET');
//    let devices = [];
//    for(let i = 0; i < allDev.length; i++) {
//      let dev = allDev[i];
//      if(dev.gatewayId === gwId) {
//        devices.push(dev);
//      }
//    }
//    
//    return Promise.resolve(devices);
  }
  
  function getAllUsers() {
    return sendRequest('/user/list', null, 'GET');
  }
  
  function sendRequest(url, data, method) {
    
    method = method || 'POST';
    
    let promise = new Promise(function(resolve, reject) {
      
      let params = {
        headers: { 
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        url: url,
        method: method,
        //data: dataFormatted,
        dateType: 'json',
        //beforeSend: function (xhr) {   //Include the bearer token in header
        //  xhr.setRequestHeader("Authorization", 'Bearer '+ getToken());
        //}
      };
      
      if(data) {
        let dataFormatted = data;
        if(typeof data !== 'string') {
          dataFormatted = JSON.stringify(data);
        }
        
        params.data = dataFormatted;
      }
      
      $.ajax(params).done(function(response) {
         resolve(response);
      }).fail(function(err) {
        reject(err);
      })
    });
    
    return promise;
    
  }
  
  function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }
  
  function setCookie(name,value,days) {
      var expires = "";
      if (days) {
          var date = new Date();
          date.setTime(date.getTime() + (days*24*60*60*1000));
          expires = "; expires=" + date.toUTCString();
      }
      document.cookie = name + "=" + (value || "")  + expires + "; path=/";
  }
  function getCookie(name) {
      var nameEQ = name + "=";
      var ca = document.cookie.split(';');
      for(var i=0;i < ca.length;i++) {
          var c = ca[i];
          while (c.charAt(0)==' ') c = c.substring(1,c.length);
          if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
      }
      return null;
  }
  function eraseCookie(name) {   
      document.cookie = name+'=; Max-Age=-99999999;';  
  }
  
  function isAdmin() {
    let loginUser = getCookie('bob-usr-login');
    return (loginUser === 'admin');
  }
  
  function getToken() {
    return getCookie('bob-usr-token');
  }
  
  function getCurrentLoginuser() {
    return getCookie('bob-usr-login');
  }
  
  function cleanCookie() {
    eraseCookie('bob-usr-login');
    eraseCookie('bob-usr-token');
  }
  
  function emailIsValid (email) {
    return /\S+@\S+\.\S+/.test(email)
  }
  
  return service;
}