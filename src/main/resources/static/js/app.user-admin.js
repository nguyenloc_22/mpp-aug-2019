(function() {

    let log = console.log;
    let appUtil = new AppUtil();

    let allUsers = [];
    let selectedUser;
    let selectedGw = {};
    let allGws = []; 

    function startApp() {
        log('Starting User Admin Application!');
        bindEvent();
        loadData();
    }

    function loadData() {
        loadUsers();
        loadAllGWs();
    }

    function bindEvent() {
        $('#btn-create-user').bind('click', onButtonAddUserClick);
        $('#btn-reset-pwd').bind('click', onButtonResetPwdClick);
        $('#btn-delete-user').bind('click', onButtonDeleteUserClick);    
        $('#btnAddGwToUserSubmit').bind('click', linkGwToUser);
        
        $('#btnSignout').bind('click', function() {
          appUtil.cleanCookie();
          window.location.href = '/';
        });
    }
    
    function linkGwToUser() {
      
      if(!selectedUser || !selectedUser.username || !selectedGw.gatewayId) {
        toastr.error('Please select a GW');
        return;
      }
      
      appUtil.addGwToUser(selectedGw.gatewayId, selectedUser.username)
      .then(function(resp) {
        toastr.success('Added '+selectedGw.gatewayId+' to user '+ selectedUser.username, 'Info!');
      }).catch(function(err) {
        toastr.error('An error occurs. Please try again later.');
      });
    }
    
    function onButtonDeleteUserClick(e) {
      toastr.error('To be implemented', 'Weo!');
      e.preventDefault();
    }
    
    function onButtonAddUserClick(e) {
      let email = $('#txtEmail').val();
      let pwd = $('#txtPwd').val();
      if(!email || !pwd) {
        toastr.error('Please enter your email and password.', 'Failed!');
        return;
      }
      
      if(!appUtil.emailIsValid(email)) {
        toastr.error('Please enter an valid email.', 'Failed!');
        return;
      }
      
      appUtil.sendRequest('/user/create', {
        userName: email,
        password: pwd
      }).then(function(resp) {
        toastr.success('Created new user.', 'Success!');
      }).catch(function(err) {
        let errMsg = 'An error occurs while creating user. ';
        if(err && err.responseText) {
          errMsg += err.responseText;
        }
        toastr.error(errMsg, 'Error');  
      });
      
      e.preventDefault();
    }

    function onButtonResetPwdClick(e) {
      toastr.error('Reset pwd -> Not implemented.', 'Failed');
      e.preventDefault();
    }

    function loadUsers(user) {
        appUtil.getAllUsers()
        .then(function(users) {
          allUsers = users;
          ui_createUsersList(users);
        })
        .catch(console.error);
    }
    
    function loadAllGWs() {
      appUtil.getAllGateways()
      .then(function(gws) {
        allGws = gws;
        let lsGwItem = $('#lsGwsSelection');
        $(lsGwItem).empty();
        for(let i = 0; i < allGws.length; i++) {
          let gwItem = $('<div class="dropdown-item gw-dropdown-item" data-gwid="'+ allGws[i].gatewayId +'">'+ allGws[i].name +'</div>');
          $(lsGwItem).append(gwItem);
          $(gwItem).bind('click', onGwDropboxSelected);
        }
      });
    }
    
    function onGwDropboxSelected(e) {
      //let gwId = e.target.innerText;
      let gwId = e.target.attributes['data-gwid'].value;
      
      let userId = selectedUser.username;
      selectedGw.gatewayId = gwId;
      console.log(gwId);
      
      $('#btnSelectGw').html(gwId);
    }

    function ui_createUsersList(users) {
        let lsUser = $('#listUser');

        for(let i = 0; i < users.length; i++) {
            let user = users[i];

            (function(user) {
                let elemHtml = ui_createUserElement(user);
                let elem = $(elemHtml);
                lsUser.append(elem);
                $(elem).on('click', ui_onUserSelected);
            })(user);
        }

        if(allUsers && allUsers.length > 0) {
            $('.list-group-item:nth-child(2)').addClass('active');
            selectedUser = allUsers[0];
            loadGwsByUser(selectedUser.username);
        }
    }

    function selectUser(user) {
        let domId = usernameToDomId(user.username);
        $('#' + domId).addClass('active');
        selectedUser = user;
    }
    
    function ui_createUserElement(user) {
        let domId = usernameToDomId(user.username);
      
        return '<a id="'+ domId +'" data-email="'+ user.username +'" href="#" class="list-group-item list-group-item-action disabled">'
            + user.username+ '<i class="fa fa-angle-double-right fa-fw pull-right"></i></a>';
    }

    function ui_onUserSelected(e) {
        $('.list-group-item.active').removeClass('active');
        //let username = e.target.attributes['data-email'].value;
        let username = $(e.target).attr('data-email');
        let user = findUserByUsername(username);
        selectUser(user);
        $('#lblSelectedUser').html(user.username);
        $('#lblFirstName').html(user.firstName);
        $('#lblLastName').html(user.lastName);
        $('#lblEmail').html(user.email);
        loadGwsByUser(username);

        e.preventDefault();
    }
    
    function usernameToDomId(username) {
      let rs = username.split('@').join('');
      rs = rs.split('.').join('');
      
      return rs;
    }
    
    function loadGwsByUser(username) {
      appUtil.getGWsByUser(username)
      .then(function(gws) {
        console.log('Gws by user -> ', gws);
        ui_createGwsList(gws);
      })
      .catch(console.error);
    }
    
    function ui_createGwsList(gws) {
      let lsGw = $('#listGw');
      $("#listGw a:gt(0)").remove();
      //lsGw.empty();

      for(let i = 0; i < gws.length; i++) {
          let gw = gws[i];

          (function(gateway) {
              let elemHtml = ui_createGwElement(gateway);
              let elem = $(elemHtml);
              lsGw.append(elem);
              
              $(elem).find('#btnRemoveGwFromUser').on('click', onBtnRemoveGwFromUserClicked);
          })(gw);
      }
    }
    
    function onBtnRemoveGwFromUserClicked(event) {
      let username = selectedUser.username;
      let gatewayId = $(event.target).parent().attr('id');
      
      let confirmed = confirm('Do you want to unlink Bob[' + gatewayId+ '] from ' + username);
      if(!confirmed) {
        return false;
      }
      
      appUtil.removeGwFromUser(username, gatewayId)
      .then(function(resp) {
        toastr.success('Removed gw '+ gatewayId +' from user ' + username, 'Info!');
      })
      .catch(function(err) {
        toastr.error('An error occurs while removing gw from user', 'Error!');        
      })
      
      event.preventDefault();
    }
    
    function ui_createGwElement(gw) {
      return '<a id="'+ gw.gatewayId +'" href="#" class="list-group-item list-group-item-action disabled">'
          + '<span class="gw-name">'
          +    gw.name+ '</span><i id="btnRemoveGwFromUser" class="fa fa-trash pull-right" style="color: red;"></i></a>';
    }
  
    function ui_onGwSelected(e) {
      toastr.success('What are you doing?', 'Success');
    }

    function findUserByUsername(username) {
        let found = null;

        for(let i = 0; i < allUsers.length; i++) {
            let user = allUsers[i];
            if(user.username === username) {
                found = user;
                break;
            }
        }

        return found;
    }

    setTimeout(startApp, 10);

})();