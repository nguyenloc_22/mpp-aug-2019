<%--
  Created by IntelliJ IDEA.
  User: huyduong
  Date: 9/10/19
  Time: 5:50 PM
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="https://templates.pingendo.com/assets/Pingendo_favicon.ico">
    <title>Change Password - Bob Group</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <c:url value="/css/login/login.css" var="jstlCss" />
    <link href="${jstlCss}" rel="stylesheet" />
    <link href="/toast/toastr.min.css" rel="stylesheet" />
    <script src="/js/navbar-ontop.js"></script>
    <script src="/js/animate-in.js"></script>
</head>
<body>
<!-- Navbar -->
<nav class="navbar navbar-expand-md navbar-dark bg-primary fixed-top">
    <div class="container"> <a class="navbar-brand" href="/dashboard">
        <i class="fa d-inline fa-lg fa-stop-circle"></i>
        <b> BOB TEAM</b>
    </a> <button class="navbar-toggler navbar-toggler-right border-0" type="button" data-toggle="collapse" data-target="#navbar16">
        <span class="navbar-toggler-icon"></span>
    </button>
        <div class="collapse navbar-collapse" id="navbar16">
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a href="#" class="btn dropdown-toggle ml-md-2 btn-light text-dark" data-toggle="dropdown">ACCOUNT</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item">Profile</a>
                        <a href="#" class="dropdown-item">Settings</a>
                        <a href="#"class="dropdown-item">Sign out</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Body -->
<div class="d-flex cover section-aquamarine py-5 align-items-center flex-column" style="background-image: url(&quot;assets/background_main.jpg&quot;);">
    <div class="container">
        <div class="row"></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 p-3">
                <form class="p-4 bg-light" method="post">
                    <h1 class="mb-4 text-center">CHANGE PASSWORD</h1>
                    <div class="form-group"> <label>Old password</label>
                        <input class="form-control" type="password" id="txt-old-password" required="required"> </div>
                    <div class="form-group"> <label>New password</label>
                        <input class="form-control" type="password" id="txt-new-password" required="required"> </div>
                    <div class="form-group"> <label>Confirm new password</label>
                        <input class="form-control" type="password" id="txt-confirm-new-password" required="required"> </div>
                    <button type="button" class="btn mt-4 btn-block p-2 btn-primary" id="btn-change-password"><b>UPDATE PASSWORD</b></button>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="toast/toastr.min.js"></script>
<script src="js/smooth-scroll.js" style=""></script>
<script src="js/app.constant.js" style=""></script>
<script src="js/app.util.js" style=""></script>
<script src="js/app.change-password.js" style=""></script>
</body>
</html>
