<%--
  Created by IntelliJ IDEA.
  User: huyduong
  Date: 9/10/19
  Time: 5:04 PM
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/assets/bob-icon.ico">
    <link rel="icon" href="https://templates.pingendo.com/assets/Pingendo_favicon.ico">
    <title>Dashboard - Bob Group</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link href="/toast/toastr.min.css" rel="stylesheet" />
    <c:url value="/css/login/login.css" var="jstlCss" />
    <link href="${jstlCss}" rel="stylesheet" />
    <c:url value="/css/main.css" var="jstlMainCss" />
    <link href="${jstlMainCss}" rel="stylesheet" />
    <style type="text/css">
    	.switch-container {
    		margin-bottom: 10px;
    	}
    </style>
</head>

<body>
<!-- Navbar -->
<nav class="navbar navbar-expand-md navbar-dark bg-primary fixed-top">
    <div class="container"> <a class="navbar-brand" href="#">
        <i class="fa d-inline fa-lg fa-stop-circle"></i>
        <b> BOB TEAM</b>
    </a> <button class="navbar-toggler navbar-toggler-right border-0" type="button" data-toggle="collapse" data-target="#navbar16">
        <span class="navbar-toggler-icon"></span>
    </button>
        <div class="collapse navbar-collapse" id="navbar16">
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a href="#" class="btn dropdown-toggle ml-md-2 btn-light text-dark" data-toggle="dropdown">ACCOUNT</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="/profile" class="dropdown-item">Profile</a>
                        <a href="/user-admin" class="item-admin dropdown-item">Users</a>
                        <a href="/gateway-admin" class="item-admin dropdown-item">Gateways</a>
                        <a href="/dev-statistic" class="item-admin dropdown-item">Statistic</a>
                        <a id="btnSignout" href="#" class="dropdown-item">Sign out</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Body -->
<div class="d-flex cover section-aquamarine py-5 align-items-start flex-column" style="background-image: url(&quot;assets/background_main.jpg&quot;);">
    <div class="container">
        <div class="row mt-3 mb-5">
            <div class="col-md-12 text-center">
                <h1 class="heading" contenteditable="false">DASHBOARD</h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div id="listGw" class="list-group">
                    <a href="#" class="list-group-item list-group-item-action list-header"> Gateway</a>
                </div>
            </div>
            <div class="col-md-6">
                <div id="lsDevices" class="row">
                </div>
            </div>
        </div>
    </div>
    <div class="container"></div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="/js/socket.io/socket.io.js"></script>
<script src="/js/navbar-ontop.js"></script>
<script src="/js/animate-in.js"></script>
<script src="js/smooth-scroll.js" style=""></script>
<script src="toast/toastr.min.js" style=""></script>
<script src="js/app.constant.js" style=""></script>
<script src="js/app.util.js" style=""></script>
<script src="js/app.socket.js" style=""></script>
<script src="js/app.dashboard.js" style=""></script>
</body>

</html>
