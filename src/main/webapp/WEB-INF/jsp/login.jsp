<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="https://templates.pingendo.com/assets/Pingendo_favicon.ico">
  <title>Bob Group</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
  <c:url value="/css/login/login.css" var="jstlCss" />
  <link href="${jstlCss}" rel="stylesheet" />
  <link href="/toast/toastr.min.css" rel="stylesheet" />
  <script src="/js/navbar-ontop.js"></script>
  <script src="/js/animate-in.js"></script>
</head>

<body>
<!-- Navbar -->
<nav class="navbar navbar-expand-md navbar-dark bg-primary fixed-top">
  <div class="container">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar3SupportedContent" aria-controls="navbar3SupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
    <div class="collapse navbar-collapse text-center justify-content-center" id="navbar3SupportedContent">
      <ul class="navbar-nav">
        <li class="nav-item mx-3">
          <a class="nav-link text-light" href="#"><b>HOME</b></a>
        </li>
        <li class="nav-item mx-2">
          <a class="nav-link" href="#devices"><b>DEVICES</b></a>
        </li>
        <li class="nav-item mx-2">
          <a class="nav-link" href="#about"><b>ABOUT</b></a>
        </li>
      </ul>
    </div>
  </div>
</nav>

<!-- Body -->
<div class="align-items-center d-flex cover section-aquamarine py-5" style="background-image: url(&quot;assets/background_main.jpg&quot;);">
  <div class="container">
    <div class="row">
      <div class="col-lg-7 align-self-center text-lg-left text-center">
        <h1 class="mb-0 mt-5 display-4">CS401 - PROJECT</h1>
        <p class="mb-5">Bob Group</p>
      </div>
      <div class="col-lg-5 p-3">
        <div class="p-4 bg-light">
          <h4 class="mb-4 text-center">LOGIN</h4>
          <div class="form-group"> <label>Email - Username&nbsp;</label>
            <input class="form-control" id="email" required="required"> </div>
          <div class="form-group"> <label>Password</label>
            <input type="password" class="form-control" id="pwd" required="required"> </div>
          <button type="button" class="btn mt-4 btn-block p-2 btn-primary" id="buttonLogin"><b>LOGIN</b></button>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="py-5 bg-light" id="devices" style="">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <p class="m-0 text-center">We are using the following devices for this demo</p>
        <h2 class="mb-4 text-primary text-center">Amazon Stores</h2>
        <div class="row">
          <div class="col-md-4 p-3">
            <img class="img-fluid d-block w-100 mb-3" src="assets/gateway_raspberry.jpg">
            <h5 class=""><b>Raspberry</b></h5>
            <p class="mb-3">1.4GHz 64-bit quad-core processor, dual-band wireless LAN, Bluetooth 4.2/BLE, faster Ethernet...</p>
            <a href="https://www.amazon.com/Raspberry-Pi-MS-004-00000024-Model-Board/dp/B01LPLPBS8/ref=asc_df_B01LPLPBS8/?tag=hyprod-20&amp;linkCode=df0&amp;hvadid=309776868400&amp;hvpos=1o1&amp;hvnetw=g&amp;hvrand=15287176572700604964&amp;hvpone=&amp;hvptwo=&amp;hvqmt=&amp;hvdev=c&amp;hvdvcmdl=&amp;hvlocint=&amp;hvlocphy=9018597&amp;hvtargid=aud-829758849484:pla-521496176766&amp;psc=1" class="btn btn-outline-primary" target="_blank"><b class="">Amazon</b></a>
          </div>
          <div class="col-md-4 p-3">
            <img class="img-fluid d-block w-100 mb-3" src="assets/gateway_edgelec.jpg">
            <h5 class="text-dark"><b>EDGELEC</b></h5>
            <p class="mb-3">120pcs Breadboard Jumper Wires 10cm 15cm 20cm 30cm 40cm 50cm 100cm Optional Arduino Wire...</p>
            <a class="btn btn-outline-primary" href="https://www.amazon.com/gp/product/B07GD2BWPY/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&amp;psc=1" target="_blank"><b class="">Amazon</b></a>
          </div>
          <div class="col-md-4 p-3">
            <img class="img-fluid d-block w-100 mb-3" src="assets/gateway_hiletgo.jpg">
            <h5 class=""><b>HiLetgo</b></h5>
            <p class="mb-3">2pcs 5V One Channel Relay Module Relay Switch with OPTO Isolation High Low Level Trigger.</p>
            <a href="https://www.amazon.com/gp/product/B00LW15A4W/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&amp;psc=1" class="btn btn-outline-primary" target="_blank"><b class="">Amazon</b></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="py-5" id="about">
  <div class="container">
    <div class="row bg-primary animate-in-down">
      <div class="p-4 col-md-6 align-self-center">
        <p class="m-0">About us</p>
        <h2>BOB GROUP</h2>
        <p class="my-4">We are from Vietnam.&nbsp;<br><br>We build something great for the world.</p>
        <a href="#" class="btn mb-3 p-2 btn-light">Team members</a>
      </div>
      <div class="p-0 col-md-6">
        <div class="carousel slide" data-ride="carousel" id="carousel1">
          <div class="carousel-inner" role="listbox">
            <div class="carousel-item active carousel-item-left">
              <img class="d-block img-fluid w-100" src="assets/profile_loc_nguyen.jpg" alt="first slide">
              <div class="carousel-caption">
                <h3>Van Loc, Nguyen</h3>
                <p>Backend Engineer</p>
              </div>
            </div>
            <div class="carousel-item carousel-item-next carousel-item-left">
              <img class="d-block img-fluid w-100" src="assets/profile_huy_duong.jpg" data-holder-rendered="true">
              <div class="carousel-caption">
                <h3>Ho Minh Huy, Duong</h3>
                <p>Frontend Engineer</p>
              </div>
            </div>
            <div class="carousel-item">
              <img class="d-block img-fluid w-100" src="assets/profile_dat_phan.jpg" data-holder-rendered="true">
              <div class="carousel-caption">
                <h3>Xuan Dat, Phan</h3>
                <p>Frontend Engineer</p>
              </div>
            </div>
            <div class="carousel-item">
              <img class="d-block img-fluid w-100" src="assets/profile_thong_phan.jpg" data-holder-rendered="true">
              <div class="carousel-caption">
                <h3>Duy Thong, Phan</h3>
                <p>Backend Engineer</p>
              </div>
            </div>
          </div>
          <a class="carousel-control-prev" href="#carousel1" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a>
          <a class="carousel-control-next" href="#carousel1" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="text-center bg-dark pt-5">
  <div class="container">
    <div class="row">
      <div class="col-md-4 p-3">
        <h2 class="mb-4">MUM</h2>
        <p>1000 N 4th St,&nbsp; Fairfield, IA, 52557</p>
      </div>
      <div class="col-md-4 p-3">
        <h2 class="mb-4">Class</h2>
        <p class="m-0">CS401 - MPP</p>
      </div>
      <div class="col-md-4 p-3">
        <h2 class="mb-4">Professor</h2>
        <p>Mohamed Abdelrazik</p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 mt-3">
        <p class="text-center text-muted"> © Copyright 2019 Bob Team - All rights reserved. </p>
      </div>
    </div>
  </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="toast/toastr.min.js"></script>
<script src="/js/smooth-scroll.js"></script>
<script src="/js/app.util.js" style=""></script>
<script src="/js/login.js"></script>
</body>
</html>
