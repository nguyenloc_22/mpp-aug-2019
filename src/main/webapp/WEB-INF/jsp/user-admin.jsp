<%--
  Created by IntelliJ IDEA.
  User: huyduong
  Date: 9/10/19
  Time: 6:52 PM
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/assets/bob-icon.ico">
    <title>User Admin - Bob Group</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <c:url value="/css/login/login.css" var="jstlCss" />
    <link href="${jstlCss}" rel="stylesheet" />
    <c:url value="/css/main.css" var="jstlMainCss" />
    <link href="${jstlMainCss}" rel="stylesheet" />
    <link href="/toast/toastr.min.css" rel="stylesheet" />
    <script src="/js/navbar-ontop.js"></script>
    <script src="/js/animate-in.js"></script>
</head>

<body>
<!-- Navbar -->
<nav class="navbar navbar-expand-md navbar-dark bg-primary fixed-top">
    <div class="container"> <a class="navbar-brand" href="/dashboard">
        <i class="fa d-inline fa-lg fa-stop-circle"></i>
        <b> BOB TEAM</b>
    </a> <button class="navbar-toggler navbar-toggler-right border-0" type="button" data-toggle="collapse" data-target="#navbar16">
        <span class="navbar-toggler-icon"></span>
    </button>
        <div class="collapse navbar-collapse" id="navbar16">
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a href="#" class="btn dropdown-toggle ml-md-2 btn-light text-dark" data-toggle="dropdown">ACCOUNT</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="/profile" class="dropdown-item">Profile</a>
                        <a href="/user-admin" class="dropdown-item">Users</a>
                        <a href="/gateway-admin" class="dropdown-item">Gateways</a>
                        <a id="btnSignout" href="#" class="dropdown-item">Sign out</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="d-flex cover section-aquamarine py-5 align-items-start flex-column" style="background-image: url(&quot;assets/background_main.jpg&quot;);">
    <div class="container">
        <div class="row mt-3 mb-4">
            <div class="col-md-12 text-center">
                <h1 class="heading">USER ADMIN</h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 mt-1">
            	
                <div id="listUser" class="list-group">
                	<a href="#" class="list-group-item list-group-item-action list-header"> USER</a>
                </div>
            </div>
            <div class="col-md-6 mt-1">
                <div class="list-group" id="listGw">
                	<a href="#" id="btnAddGwToUser"
                	  data-toggle="modal" data-target="#modalLinkGw"
                	  class="list-group-item list-group-item-action list-header"> Gateway
                	  <i class="fa fa-plus pull-right"></i>
               		</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container mt-3">
        <div class="row">
            <div class="col-md-12">
                <h5 class="" contenteditable="true">Current select user: <b><span id="lblSelectedUser"></span></b></h5>
            </div>
        </div>
    </div>
    <div class="container mt-1">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <h6 class="">First name: <span id="lblFirstName">Huy</span></h6>
                    </div>
                    <div class="col-md-6">
                        <h6 class="">Lastname: <span id="lblLastName">Duong</span></h6>
                    </div>
                    <div class="col-md-6">
                        <h6 class="">E-mail: <span id="lblEmail">hduong@mum.edu</span></h6>
                    </div>
                    <div class="col-md-6">
                        <h6 class="">Phone number: +1 (641) 819-1135</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container mt-2">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group"><input type="text" class="form-control" placeholder="New password"></div>
                    </div>
                    <div class="col-md-4"><button id="btn-reset-pwd" class="btn btn-primary btn-block" type="button">Reset</button></div>
                    <div class="col-md-4" style="display: none;">
                        <div class="row">
                            <div class="col-md-6"></div>
                            <div class="col-md-6"><a id="btn-delete-user" class="btn btn-danger my-1" href="#">DELETE USER</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container my-3">
        <div class="row">
            <div class="col-md-12">
                <h5 class="my-3">Create new user in the system:</h5>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group"><label>User e-mail</label><input id="txtEmail" type="email" class="form-control" placeholder="Email"></div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group"><label>User password</label><input id="txtPwd" type="password" class="form-control" placeholder="Pasword"></div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group"><label>&nbsp; &nbsp;&nbsp;</label><button id="btn-create-user" class="btn btn-primary btn-block" type="button">Create</button></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Link GW popup -->
    <div id="modalLinkGw" class="modal" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content" style="color: black;">
	      <div class="modal-header">
	        <h5 class="modal-title">Link GW to User</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <div class="dropdown">
			  <button id="btnSelectGw" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			    Select a GW
			  </button>
			  <div class="dropdown-menu" id="lsGwsSelection" aria-labelledby="dropdownMenuButton">
			    <a class="dropdown-item" href="#">Bob 001</a>
			    <a class="dropdown-item" href="#">Bob 002</a>
			    <a class="dropdown-item" href="#">Bob 003</a>
			  </div>
			</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-primary" id="btnAddGwToUserSubmit">Add</button>
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="toast/toastr.min.js"></script>
<script src="js/smooth-scroll.js" style=""></script>
<script src="js/app.constant.js" style=""></script>
<script src="js/app.util.js" style=""></script>
<script src="js/app.user-admin.js" style=""></script>
</body>
</html>
