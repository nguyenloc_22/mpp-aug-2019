<%--
  Created by IntelliJ IDEA.
  User: huyduong
  Date: 9/10/19
  Time: 5:04 PM
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/assets/bob-icon.ico">
    <!-- PAGE settings -->
    <link rel="icon" href="https://templates.pingendo.com/assets/Pingendo_favicon.ico">
    <title>Device statistic - Bob Group</title>
    <!-- CSS dependencies -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.css" />
    <!-- <link href="/chartjs/Chart.min.css" rel="stylesheet" /> -->
    <link href="/toast/toastr.min.css" rel="stylesheet" />
    <c:url value="/css/login/login.css" var="jstlCss" />
    <link href="${jstlCss}" rel="stylesheet" />
    <c:url value="/css/main.css" var="jstlMainCss" />
    <link href="${jstlMainCss}" rel="stylesheet" />
    
    <style type="text/css">
     .chart-section{
     	background: white;
     	z-index: 100;
     	color: black;
     }
     .chart-info-section{
     	z-index: 100;
     }
     </style>
</head>

<body>
<!-- Navbar -->
<!-- Cover -->
<nav class="navbar navbar-expand-md navbar-dark bg-primary fixed-top">
    <div class="container"> <a class="navbar-brand" href="/dashboard">
        <i class="fa d-inline fa-lg fa-stop-circle"></i>
        <b> BOB TEAM</b>
    </a> <button class="navbar-toggler navbar-toggler-right border-0" type="button" data-toggle="collapse" data-target="#navbar16">
        <span class="navbar-toggler-icon"></span>
    </button>
        <div class="collapse navbar-collapse" id="navbar16">
            <!-- <ul class="navbar-nav ml-auto"></ul> <a class="btn navbar-btn ml-md-2 btn-light text-dark" contenteditable="true">PROFILE</a> -->
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a href="#" class="btn dropdown-toggle ml-md-2 btn-light text-dark" data-toggle="dropdown">ACCOUNT</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="/profile" class="dropdown-item">Profile</a>
                        <a href="/user-admin" class="item-admin dropdown-item">Users</a>
                        <a href="/gateway-admin" class="item-admin dropdown-item">Gateways</a>
                        <!-- <div class="dropdown-divider"></div> -->
                        <a id="btnSignout" href="#" class="dropdown-item">Sign out</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="d-flex cover section-aquamarine py-5 align-items-start flex-column" style="background-image: url(&quot;assets/background_main.jpg&quot;);">
    <div class="container chart-info-section">
        <div class="row mt-3 mb-5">
            <div class="col-md-12 text-center">
                <h1 class="" contenteditable="false">Device Statistic</h1>
            </div>
        </div>
    </div>
    <div class="container" style="color: white; z-index: 100;">
    	<h3>This section to select a GW</h3>
   	</div>
    <div class="container">
        <div class="row" style="margin-bottom: 10px;">
            <br>
            <div class="dropdown" style="margin-right: 5px;">
			  <button id="btnSelectGw" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			    Select a GW
			  </button>
			  <div class="dropdown-menu" id="lsGwsSelection" aria-labelledby="dropdownMenuButton">
			    <a class="dropdown-item" href="#">Bob 001</a>
			    <a class="dropdown-item" href="#">Bob 002</a>
			    <a class="dropdown-item" href="#">Bob 003</a>
			  </div>
			  
			  <button id="btnLoadDeviceHistory" class="btn btn-primary">Load data</button>
			</div>
			
        </div>
    </div>
    <div class="container chart-section">
    	<div id="chartBox">
    		<canvas id="canvasId"></canvas>
    	</div>
    </div>
</div>

<!-- JavaScript dependencies -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" type="text/javascript"></script>
<script src="toast/toastr.min.js" type="text/javascript"></script>
<script src="js/chartjs/Chart.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.bundle.js" type="text/javascript"></script>

<script src="js/app.constant.js" type="text/javascript"></script>
<script src="js/app.util.js" type="text/javascript"></script>
<script src="js/app.dev-statistic.js" type="text/javascript"></script>
</body>

</html>
