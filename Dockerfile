FROM openjdk:8-jdk-alpine
VOLUME /tmp
#ARG JAR_FILE=/target/home-device-control-api-1.jar
#COPY ${JAR_FILE} api.jar
ARG JAR_FILE=/target/home-device-control-api-1.war
COPY ${JAR_FILE} api.war


EXPOSE 8080
ENTRYPOINT ["java","-jar","/api.war"]